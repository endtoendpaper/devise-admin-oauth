FactoryBot.define do
  factory :support_issue do
    user_id { FactoryBot.create(:user, :confirmed).id }
    user_email { User.find(user_id).email }
    sequence(:summary) { |n| "#{Faker::Company.catch_phrase} #{n}" }
    body { Faker::Books::Lovecraft.sentences(number: 25).join(" ") }
    status_log { 'Issue Created at ' + Time.current.strftime('%b %d, %Y %I:%M%p %Z') + ' by ' + User.find(user_id).username + ' (' + User.find(user_id).email + ")\n" }

    trait :addressed do
      addressed { true }
    end

    trait :rich_text do
      rich_text_body { Faker::Books::Lovecraft.sentences(number: 25).join(" ") }
      body { nil }
    end
  end
end

