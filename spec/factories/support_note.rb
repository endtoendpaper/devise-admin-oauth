FactoryBot.define do
  factory :support_note do
    user_id { FactoryBot.create(:user, :confirmed).id }
    user_email { User.find(user_id).email }
    body { Faker::Books::Lovecraft.sentences(number: 3).join(" ") }
    support_issue_id { FactoryBot.create(:support_issue, :rich_text).id }

    trait :rich_text do
      rich_text_body { Faker::Books::Lovecraft.sentences(number: 3).join(" ") }
      body { nil }
    end
  end
end
