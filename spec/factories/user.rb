FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "#{n}_#{Faker::Internet.email}" }
    sequence(:username) { |n| "#{Faker::Internet.username(specifier: 5..40, separators: ['_'])}_#{n}" }
    password { Faker::Internet.password }
    time_zone { ["Mountain Time (US & Canada)",
        "Europe/Moscow",
        "Asia/Tashkent",
        "Asia/Singapore",
        "Pacific/Noumea",
        "Asia/Dhaka",
        "Asia/Kolkata",
        "Pacific/Auckland",
        "Europe/Skopje",
        "Asia/Tashkent",
        "Asia/Yerevan",
        "Europe/Zagreb",
        "Europe/Skopje",
        "Pacific/Apia",
        "Pacific/Fakaofo",
        "Australia/Adelaide",
        "Pacific/Fakaofo",
        "Asia/Karachi",
        "Europe/Belgrade",
        "Pacific/Guam",
        "Europe/Lisbon",
        "Atlantic/Azores",
        "Australia/Hobart",
        "Europe/Paris",
        "Asia/Taipei",
        "Africa/Harare",
        "Asia/Tbilisi",
        "America/Mexico_City",
        "America/Mazatlan",
        "Asia/Taipei",
        "America/Juneau",
        "America/Lima",
        "Asia/Karachi",
        "Africa/Johannesburg"].shuffle.first }

    trait :confirmed do
      confirmed_at { Time.zone.now }
      confirmation_sent_at { Time.zone.now }
      confirmation_token { Faker::Number.number(digits: 10) }
    end

    trait :admin do
      admin { true }
    end

    trait :avatar do
      avatar { Rack::Test::UploadedFile
        .new('spec/factories/images/crying-cat.jpeg', 'image/jpeg') }
    end

    trait :invalid_username do
      username { 'bad username' }
    end

    trait :locked do
      locked_at { Time.zone.now }
    end

    trait :hide_relationships do
      hide_relationships { true }
    end
  end
end
