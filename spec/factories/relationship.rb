FactoryBot.define do
  factory :relationship do
    follower_id { FactoryBot.create(:user, :confirmed).id }
    followed_id { FactoryBot.create(:user, :confirmed).id }
  end
end
