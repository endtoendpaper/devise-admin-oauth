FactoryBot.define do
  factory :help_section do
    sequence(:title) { |n| "#{Faker::DcComics.title} #{n}" }
    content { Faker::Games::WarhammerFantasy.quote }
  end
end
