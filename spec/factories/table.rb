FactoryBot.define do
  factory :table do
    columns { 1 }
    rows { 1 }
    data { [{"0-0"=>"asdfasdf"}] }
    header_row { true }
    record_saved { false }
  end

  trait :help_section do
    record_id { FactoryBot.create(:help_section).id }
    record_type { 'HelpSection' }
  end

  trait :support_comment do
    record_id { FactoryBot.create(:support_comment, :rich_text).id }
    record_type { 'SupportComment' }
  end

  trait :support_issue do
    record_id { FactoryBot.create(:support_issue, :rich_text).id }
    record_type { 'SupportIssue' }
  end

  trait :support_note do
    record_id { FactoryBot.create(:support_note, :rich_text).id }
    record_type { 'SupportNote' }
  end

  trait :support_reply do
    record_id { FactoryBot.create(:support_reply, :rich_text).id }
    record_type { 'SupportReply' }
  end

  trait :updated_30_hr_ago do
    updated_at { Time.zone.now - 30.hours }
  end

  trait :updated_3_day_ago do
    updated_at { Time.zone.now - 3.days }
  end

  trait :record_saved do
    record_saved { true }
  end
end
