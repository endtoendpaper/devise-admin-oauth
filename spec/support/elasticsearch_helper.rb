def reset_elasticsearch_indices
  begin
    User.__elasticsearch__.delete_index!
  rescue
    # Ignore if the index does not exist
  end
  User.__elasticsearch__.create_index!
end

def reindex_elasticsearch_data
  User.import
  User.__elasticsearch__.refresh_index!
end
