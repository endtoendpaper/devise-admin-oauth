Capybara.register_driver :selenium_chrome_in_container do |app|
  options = Selenium::WebDriver::Chrome::Options.new
  options.add_argument("--headless")
  options.add_argument("--disable-gpu")
  options.add_argument("--window-size=1920,1080")
  options.add_argument("--no-sandbox")
  options.add_argument("--disable-dev-shm-usage")

  Capybara::Selenium::Driver.new(
    app,
    browser: :remote,
    url: "http://selenium_chrome:4444",
    options: options
  )
end

RSpec.configure do |config|
  config.before(:each, type: :system) do
    driven_by :rack_test
  end

  config.before(:each, type: :system, js: true) do
    Selenium::WebDriver.logger.ignore(:clear_local_storage, :clear_session_storage)
    Capybara.server_host = "0.0.0.0"
    Capybara.app_host = "http://#{Socket.gethostname}:#{Capybara.server_port}"
    Capybara.default_max_wait_time = 10
    driven_by :selenium_chrome_in_container
  end
end

