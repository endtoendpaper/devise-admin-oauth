Capybara.register_driver :selenium_firefox_in_container do |app|
  options = Selenium::WebDriver::Firefox::Options.new
  options.add_argument("--headless")
  options.add_argument("--disable-gpu")
  options.add_argument("--width=1920")
  options.add_argument("--height=1080")

  Capybara::Selenium::Driver.new(
    app,
    browser: :remote,
    url: "http://selenium_firefox:4444",
    options: options
  )
end

RSpec.configure do |config|
  config.before(:each, type: :system) do
    driven_by :rack_test
  end

  config.before(:each, type: :system, js: true) do
    Capybara.server_host = "0.0.0.0"
    Capybara.app_host = "http://#{Socket.gethostname}:#{Capybara.server_port}"
    driven_by :selenium_firefox_in_container
  end
end
