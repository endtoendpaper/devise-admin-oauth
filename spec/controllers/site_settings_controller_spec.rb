require 'rails_helper'

RSpec.describe SiteSettingsController, type: :controller do
  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @user = FactoryBot.create(:user, :confirmed)
    @jpg_file = Rack::Test::UploadedFile
               .new('spec/factories/images/default-avatar.jpg', 'image/jpeg')
    @png_file = Rack::Test::UploadedFile
                .new('spec/factories/images/rails-logo.png', 'image/jpeg')
    @siteSetting = SiteSetting.first
  end

  shared_context 'when admin logged in' do
    before(:each) do
      sign_in @admin
    end
  end

  shared_context 'when user logged in' do
    before(:each) do
      sign_in @user
    end
  end

  describe 'GET edit' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful' do
        get :edit, params: { id: 323 }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful' do
        get :edit, params: { id: 323 }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not logged in' do

      it 'should not be successful' do
        get :edit, params: { id: 323 }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'PATCH update' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful with valid data' do
        patch :update, params: { id: 4452,
          site_setting: { site_name: "Kool Katz 9",
          display_site_name: 'false', users_can_join: 'false',
          contact_form: 'false', rich_text_support_replies: 'false',
          rich_text_support_issues: 'false',
          email_issue_status_to_users: 'false',
          email_contact_form_to_admins: 'false' } }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(edit_site_setting_path(SiteSetting.first.id))
        expect(flash[:notice]).to match(/Site settings were successfully updated./)
        expect(@siteSetting.reload.site_name).to eq("Kool Katz 9")
        expect(@siteSetting.display_site_name).to be false
        expect(@siteSetting.users_can_join).to be false
        expect(@siteSetting.contact_form).to be false
        expect(@siteSetting.rich_text_support_replies).to be false
        expect(@siteSetting.rich_text_support_issues).to be false
        expect(@siteSetting.email_issue_status_to_users).to be false
        expect(@siteSetting.email_contact_form_to_admins).to be false
      end

      it 'should be unsuccessful with invalid data' do
        new_site_name = 'a' * 51
        patch :update, params: { id: 4452,
          site_setting: { site_name: new_site_name } }
        expect(response).to have_http_status(:unprocessable_entity)
        expect(@siteSetting.reload.site_name).to_not eq(new_site_name)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful' do
        patch :update, params: { id: 4452,
          site_setting: { site_name: "Kool Katz 9" } }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
        expect(@siteSetting.reload.site_name).to_not eq("Kool Katz 9")
      end
    end

    context 'when not logged in' do

      it 'should not be successful' do
        patch :update, params: { id: 4452,
          site_setting: { site_name: "Kool Katz 9" } }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
        expect(@siteSetting.reload.site_name).to_not eq("Kool Katz 9")
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'PUT update' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful with valid data' do
        put :update, params: { id: 4452,
          site_setting: { site_name: "Kool Katz 9",
          display_site_name: 'false', users_can_join: 'false',
          contact_form: 'false', rich_text_support_replies: 'false',
          rich_text_support_issues: 'false',
          email_issue_status_to_users: 'false',
          email_contact_form_to_admins: 'false' } }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(edit_site_setting_path(SiteSetting.first.id))
        expect(flash[:notice]).to match(/Site settings were successfully updated./)
        expect(@siteSetting.reload.site_name).to eq("Kool Katz 9")
        expect(@siteSetting.display_site_name).to be false
        expect(@siteSetting.users_can_join).to be false
        expect(@siteSetting.contact_form).to be false
        expect(@siteSetting.rich_text_support_replies).to be false
        expect(@siteSetting.rich_text_support_issues).to be false
        expect(@siteSetting.email_issue_status_to_users).to be false
        expect(@siteSetting.email_contact_form_to_admins).to be false
      end

      it 'should be unsuccessful with invalid data' do
        new_site_name = 'a' * 51
        put :update, params: { id: 4452,
          site_setting: { site_name: new_site_name } }
        expect(response).to have_http_status(:unprocessable_entity)
        expect(@siteSetting.reload.site_name).to_not eq(new_site_name)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful' do
        put :update, params: { id: 4452,
          site_setting: { site_name: "Kool Katz 9" } }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
        expect(@siteSetting.reload.site_name).to_not eq("Kool Katz 9")
      end
    end

    context 'when not logged in' do

      it 'should not be successful' do
        put :update, params: { id: 4452,
          site_setting: { site_name: "Kool Katz 9" } }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
        expect(@siteSetting.reload.site_name).to_not eq("Kool Katz 9")
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'DELETE delete_default_avatar' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful' do
        @siteSetting.update(default_avatar: @png_file)
        expect(@siteSetting.reload.default_avatar).to be_attached
        delete :delete_default_avatar, params: { id: 4452 }
        expect(response).to have_http_status(:found)
        expect(@siteSetting.reload.default_avatar).to_not be_attached
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful' do
        delete :delete_default_avatar, params: { id: 4452 }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not logged in' do

      it 'should not be successful' do
        delete :delete_default_avatar, params: { id: 4452 }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end


  describe 'DELETE delete_logo' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful' do
        @siteSetting.update(logo: @png_file)
        expect(@siteSetting.reload.logo).to be_attached
        delete :delete_logo, params: { id: 1 }
        expect(response).to have_http_status(:found)
        expect(@siteSetting.reload.logo).to_not be_attached
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful' do
        delete :delete_logo, params: { id: 4452 }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not logged in' do

      it 'should not be successful' do
        delete :delete_logo, params: { id: 4452 }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'DELETE delete_favicon' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful' do
        @siteSetting.update(favicon: @png_file)
        expect(@siteSetting.reload.favicon).to be_attached
        delete :delete_favicon, params: { id: 1 }
        expect(response).to have_http_status(:found)
        expect(@siteSetting.reload.favicon).to_not be_attached
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful' do
        delete :delete_favicon, params: { id: 4452 }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not logged in' do

      it 'should not be successful' do
        delete :delete_favicon, params: { id: 4452 }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'DELETE delete_apple_touch_icon' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful' do
        @siteSetting.update(apple_touch_icon: @png_file)
        expect(@siteSetting.reload.apple_touch_icon).to be_attached
        delete :delete_apple_touch_icon, params: { id: 1 }
        expect(response).to have_http_status(:found)
        expect(@siteSetting.reload.apple_touch_icon).to_not be_attached
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful' do
        delete :delete_apple_touch_icon, params: { id: 4452 }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not logged in' do

      it 'should not be successful' do
        delete :delete_apple_touch_icon, params: { id: 4452 }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end
end
