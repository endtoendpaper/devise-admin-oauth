require 'rails_helper'

RSpec.describe HelpSectionsController, type: :controller do
  let(:title) { Faker::Lorem.sentence(word_count: 1, supplemental: false, random_words_to_add: 3) }
  let(:content) { Faker::Lorem.paragraph(sentence_count: 3, supplemental: false, random_sentences_to_add: 6) }

  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @user = FactoryBot.create(:user, :confirmed)
    @help_section = FactoryBot.create(:help_section)
  end

  shared_context 'when admin logged in' do
    before(:each) do
      sign_in @admin
    end
  end

  shared_context 'when user logged in' do
    before(:each) do
      sign_in @user
    end
  end

  describe 'GET new' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful' do
        get :new
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful' do
        get :new
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not logged in' do
      it 'should not be successful' do
        get :new
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'POST create' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful with valid data' do
        expect do
          post :create, params: { help_section: { title: title, content: content } }
        end.to change(HelpSection, :count).by(1)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(help_path)
        expect(flash[:notice]).to match(/Help section was successfully created./)
      end

      it 'should not be successful with invalid data' do
        expect do
          post :create, params: { help_section: { title: title } }
        end.to change(HelpSection, :count).by(0)
        expect(response).to have_http_status(:unprocessable_entity)
      end

    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful with valid data' do
        expect do
          post :create, params: { help_section: { title: title, content: content } }
        end.to change(HelpSection, :count).by(0)
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not logged in' do

      it 'should not be successful with valid data' do
        expect do
          post :create, params: { help_section: { title: title, content: content } }
        end.to change(HelpSection, :count).by(0)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'GET edit' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful' do
        get :edit, params: { id: @help_section.id }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful' do
        get :edit, params: { id: @help_section.id }
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not logged in' do
      it 'should not be successful' do
        get :edit, params: { id: @help_section.id }
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'PATCH update' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful with valid data' do
        patch :update, params: { id: @help_section.id, help_section: { title: title, content: content } }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(help_path)
        expect(flash[:notice]).to match(/Help section was successfully updated./)
        expect(@help_section.reload.title).to eq(title)
      end

      it 'should not be successful with invalid data' do
        patch :update, params: { id: @help_section.id, help_section: { title: "", content: content } }
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful with valid data' do
        patch :update, params: { id: @help_section.id, help_section: { title: title, content: content } }
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not logged in' do

      it 'should not be successful with valid data' do
        patch :update, params: { id: @help_section.id, help_section: { title: title, content: content } }
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'PUT update' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful with valid data' do
        put :update, params: { id: @help_section.id, help_section: { title: title, content: content } }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(help_path)
        expect(flash[:notice]).to match(/Help section was successfully updated./)
        expect(@help_section.reload.title).to eq(title)
      end

      it 'should not be successful with invalid data' do
        put :update, params: { id: @help_section.id, help_section: { title: "", content: content } }
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful with valid data' do
        put :update, params: { id: @help_section.id, help_section: { title: title, content: content } }
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not logged in' do

      it 'should not be successful with valid data' do
        put :update, params: { id: @help_section.id, help_section: { title: title, content: content } }
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'DELETE destroy' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful' do
        expect do
          delete :destroy, params: { id: @help_section.id }
        end.to change(HelpSection, :count).by(-1)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(help_path)
        expect(flash[:notice]).to match(/Help section was successfully deleted./)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful' do
        expect do
          delete :destroy, params: { id: @help_section.id }
        end.to change(HelpSection, :count).by(0)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not logged in' do

      it 'should not be successful' do
        expect do
          delete :destroy, params: { id: @help_section.id }
        end.to change(User, :count).by(0)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end
end
