require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  before(:each) do
    @png_file = Rack::Test::UploadedFile
                .new('spec/factories/images/rails-logo.png', 'image/jpeg')
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @user1 = FactoryBot.create(:user, :confirmed)
    @user2 = FactoryBot.create(:user, :confirmed)
    @user3 = FactoryBot.create(:user, :confirmed, :hide_relationships)
  end

  shared_context 'when admin logged in' do
    before(:each) do
      sign_in @admin
    end
  end

  shared_context 'when user logged in' do
    before(:each) do
      sign_in @user1
    end
  end

  describe 'GET index' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful' do
        get :index
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'be successful' do
        get :index
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end

    end

    context 'when not logged in' do

      it 'should not be successful' do
        get :index
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end
    end
  end

  describe 'GET show' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful' do
        get :show, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should be successful' do
        get :show, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end
    end

    context 'when not logged in' do

      it 'should be successful' do
        get :show, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end
    end
  end

  describe 'GET new' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful' do
        get :new
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful' do
        get :new
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not logged in' do

      it 'should not be successful' do
        get :new
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'GET edit' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful' do
        get :edit, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful' do
        get :edit, params: { id: @user1.username }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not logged in' do

      it 'should not be successful' do
        get :edit, params: { id: @user2.username }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'PATCH update' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful with valid data' do
        patch :update, params: { id: @user1.username, user: { username: 'me-meme' } }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(user_path(@user1.reload))
        expect(flash[:notice]).to match(/User was successfully updated./)
        expect(@user1.reload.username).to eq("me-meme")
      end

      it 'should not be successful invalid data' do
        patch :update, params: { id: @user1.reload.username, user: { username: 'me me/me' } }
        expect(response).to have_http_status(:unprocessable_entity)
        expect(@user1.reload.username).to_not eq("me me/me")
      end

      it 'should update admin attribute' do
        expect(@user2.admin).to be false
        patch :update, params: { id: @user2.username, user: { admin: 'true' } }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(user_path(@user2))
        expect(flash[:notice]).to match(/User was successfully updated./)
        expect(@user2.reload.admin).to be true
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful' do
        patch :update, params: { id: @user1.username, user: { username: 'me-me/me' } }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not logged in' do

      it 'should not be successful' do
        patch :update, params: { id: @user1.username, user: { username: 'me-me/me' } }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'PUT update' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful with valid data' do
        put :update, params: { id: @user1.username, user: { username: 'me-meme' } }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(user_path(@user1.reload))
        expect(flash[:notice]).to match(/User was successfully updated./)
        expect(@user1.reload.username).to eq("me-meme")
      end

      it 'should not be successful invalid data' do
        put :update, params: { id: @user1.reload.username, user: { username: 'me me/me' } }
        expect(response).to have_http_status(:unprocessable_entity)
        expect(@user1.reload.username).to_not eq("me-me/me")
      end

      it 'should update admin attribute' do
        @user2.update(admin: 'false')
        expect(@user2.reload.admin).to be false
        put :update, params: { id: @user2.username, user: { admin: 'true' } }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(user_path(@user2))
        expect(flash[:notice]).to match(/User was successfully updated./)
        expect(@user2.reload.admin).to be true
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful' do
        put :update, params: { id: @user1.username, user: { username: 'me-me/me' } }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not logged in' do

      it 'should not be successful' do
        put :update, params: { id: @user1.username, user: { username: 'me-me/me' } }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'DELETE destroy' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful' do
        expect do
          delete :destroy, params: { id: @user1.reload.username }
        end.to change(User, :count).by(-1)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(users_path)
        expect(flash[:notice]).to match(/User was successfully deleted./)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful' do
        expect do
          delete :destroy, params: { id: @user1.username }
        end.to change(User, :count).by(0)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not logged in' do

      it 'should not be successful' do
      expect do
        delete :destroy, params: { id: @user1.username }
      end.to change(User, :count).by(0)
      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(new_user_session_path)
      expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
    end

    end
  end

  describe 'DELETE delete_avatar' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful' do
        @user1.update(avatar: @png_file)
        expect(@user1.reload.avatar).to be_attached
        delete :delete_avatar, params: { id: @user1.username }
        expect(response).to have_http_status(:found)
        expect(@user1.reload.avatar).to_not be_attached
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful' do
        @user1.update(avatar: @png_file)
        expect(@user1.reload.avatar).to be_attached
        delete :delete_avatar, params: { id: @user1.username }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
        expect(@user1.reload.avatar).to be_attached
      end
    end

    context 'when not logged in' do

      it 'should not be successful' do
        @user1.update(avatar: @png_file)
        expect(@user1.reload.avatar).to be_attached
        delete :delete_avatar, params: { id: @user1.username }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
        expect(@user1.reload.avatar).to be_attached
      end
    end
  end

  describe 'GET following' do
    context 'for admin' do
      include_context 'when admin logged in'

      it 'should be successful when follow is enabled in site settings' do
        SiteSetting.first.update(follow_users: true)
        get :following, params: { id: @user1.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end

      it 'should be successful when user selected to hide follow info' do
        SiteSetting.first.update(follow_users: true)
        get :following, params: { id: @user3.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end

      it 'should not be successful when follow is disabled in site settings' do
        SiteSetting.first.update(follow_users: false)
        get :following, params: { id: @user1.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
        expect(response).to render_template('errors/not_found')
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should be successful when follow is enabled in site settings' do
        SiteSetting.first.update(follow_users: true)
        get :following, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end

      it 'should be successful when user selected to hide their follow info' do
        SiteSetting.first.update(follow_users: true)
        @user1.update(hide_relationships: true)
        get :following, params: { id: @user1.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end

      it 'should not be successful for another user who selected to hide follow data' do
        SiteSetting.first.update(follow_users: true)
        get :following, params: { id: @user3.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
        expect(response).to render_template('errors/not_found')
      end

      it 'should not be successful when follow is disabled in site settings' do
        SiteSetting.first.update(follow_users: false)
        get :following, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
        expect(response).to render_template('errors/not_found')
      end
    end

    context 'when not logged in' do

      it 'should be successful when follow is enabled in site settings' do
        SiteSetting.first.update(follow_users: true)
        get :following, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end

      it 'should not be successful for user who selected to hide follow data' do
        SiteSetting.first.update(follow_users: true)
        get :following, params: { id: @user3.username }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
        expect(flash[:alert]).to be_nil
      end

      it 'should not be successful when follow is disabled in site settings' do
        SiteSetting.first.update(follow_users: false)
        get :following, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
        expect(flash[:alert]).to be_nil
      end
    end
  end

  describe 'GET followers' do
    context 'for admin' do
      include_context 'when admin logged in'

      it 'should be successful when follow is enabled in site settings' do
        SiteSetting.first.update(follow_users: true)
        get :followers, params: { id: @user1.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end

      it 'should be successful when user selected to hide follow info' do
        SiteSetting.first.update(follow_users: true)
        get :followers, params: { id: @user3.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end

      it 'should not be successful when follow is disabled in site settings' do
        SiteSetting.first.update(follow_users: false)
        get :followers, params: { id: @user1.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
        expect(response).to render_template('errors/not_found')
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should be successful when follow is enabled in site settings' do
        SiteSetting.first.update(follow_users: true)
        get :followers, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end

      it 'should be successful when user selected to hide their follow info' do
        SiteSetting.first.update(follow_users: true)
        @user1.update(hide_relationships: true)
        get :followers, params: { id: @user1.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end

      it 'should not be successful for another user who selected to hide follow data' do
        SiteSetting.first.update(follow_users: true)
        get :followers, params: { id: @user3.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
        expect(response).to render_template('errors/not_found')
      end

      it 'should not be successful when follow is disabled in site settings' do
        SiteSetting.first.update(follow_users: false)
        get :followers, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
        expect(response).to render_template('errors/not_found')
      end
    end

    context 'when not logged in' do

      it 'should be successful when follow is enabled in site settings' do
        SiteSetting.first.update(follow_users: true)
        get :followers, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end

      it 'should not be successful for user who selected to hide follow data' do
        SiteSetting.first.update(follow_users: true)
        get :followers, params: { id: @user3.username }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
        expect(flash[:alert]).to be_nil
      end

      it 'should not be successful when follow is disabled in site settings' do
        SiteSetting.first.update(follow_users: false)
        get :followers, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
        expect(flash[:alert]).to be_nil
      end
    end
  end
end
