require 'rails_helper'

RSpec.describe SupportIssuesController, type: :controller do
  let(:summary) { Faker::Lorem.sentence(word_count: 8) }
  let(:body) { Faker::Lorem.sentence(word_count: 200) }

  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @admin2 = FactoryBot.create(:user, :confirmed, :admin)
    @user1 = FactoryBot.create(:user, :confirmed)
    @user2 = FactoryBot.create(:user, :confirmed)
    @support_issue = FactoryBot.create(:support_issue, :rich_text, user_id: @user1.id)
  end

  shared_context 'when admin logged in' do
    before(:each) do
      sign_in @admin
    end
  end

  shared_context 'when user logged in' do
    before(:each) do
      sign_in @user1
    end
  end

  describe 'GET user support index' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful' do
        get :index, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end
    end

    context 'for user' do
      it 'should be successful for own index' do
        sign_in @user2
        get :index, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end
    end

    context 'for another user' do
      include_context 'when user logged in'

      it 'should not be successful' do
        get :index, params: { id: @user2.username }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end
    end

    context 'when not logged in' do
      it 'should not be successful' do
        get :index, params: { id: @user2.username }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'GET index' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful' do
        get :index
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful' do
        get :index
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end
    end

    context 'when not logged in' do
      it 'should not be successful' do
        get :index
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'GET show' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful' do
        get :show, params: { id: @support_issue.id }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end
    end

    context 'for user' do
      it 'should not be successful for another user issue' do
        sign_in @user2
        get :show, params: { id: @support_issue.id }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template('errors/not_found')
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should be successful for own issue' do
        get :show, params: { id: @support_issue.id }
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end
    end

    context 'when not logged in' do
      it 'should not be successful' do
        get :show, params: { id: @support_issue.id }
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'GET new' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful' do
        get :new
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should be successful' do
        get :new
        expect(response).to have_http_status(:ok)
        expect(flash[:alert]).to be_nil
      end
    end

    context 'when not logged in' do
      it 'should not be successful' do
        get :new
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'POST create' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful with valid data' do
        SiteSetting.first.update(email_contact_form_to_admins: 'true')
        SiteSetting.first.update(email_issue_status_to_users: 'true')
        expect do
          post :create, params: { support_issue: { summary: summary, rich_text_body: body } }
        end.to change(SupportIssue, :count).by(1).and change { ActionMailer::Base.deliveries.size }.by(2)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(help_path)
        expect(flash[:notice]).to match(/Support form successfully submitted./)
        expect(SupportIssue.last.addressed).to be false
        expect(SupportIssue.last.user_email).to eq(@admin.email)
        expect(SupportIssue.last.user_id).to eq(@admin.id)
        expect(SupportIssue.last.status_log).to include("Issue Created at")
      end

      it 'should not be successful with invalid data' do
        long_summary = 'a' * 256
        expect do
          post :create, params: { support_issue: { summary: long_summary, body: body } }
        end.to change(SupportIssue, :count).by(0).and change { ActionMailer::Base.deliveries.size }.by(0)
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'should not send admin mailer if email_admins_contact_form? is false' do
        SiteSetting.first.update(email_contact_form_to_admins: 'false')
        SiteSetting.first.update(email_issue_status_to_users: 'true')
        expect do
          post :create, params: { support_issue: { summary: summary, rich_text_body: body } }
        end.to change(SupportIssue, :count).by(1).and change { ActionMailer::Base.deliveries.size }.by(1)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(help_path)
        expect(ActionMailer::Base.deliveries.last.body).to render_template('user_mailer/support_issue_created')
        expect(ActionMailer::Base.deliveries.last.subject).to eq(SiteSetting.site_name + ' ' + SupportIssue.last.display_name + ': ' + SupportIssue.last.summary)
      end

      it 'should not send user mailer if email_issue_status_to_users? is false' do
        SiteSetting.first.update(email_contact_form_to_admins: 'true')
        SiteSetting.first.update(email_issue_status_to_users: 'false')
        expect do
          post :create, params: { support_issue: { summary: summary, rich_text_body: body } }
        end.to change(SupportIssue, :count).by(1).and change { ActionMailer::Base.deliveries.size }.by(1)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(help_path)
        expect(ActionMailer::Base.deliveries.last.body).to render_template('admins_mailer/support_issue_created')
        expect(ActionMailer::Base.deliveries.last.subject).to eq(SiteSetting.site_name + ' ' + SupportIssue.last.display_name + ': ' + SupportIssue.last.summary)
      end

      it 'does not updated addressed field' do
        expect do
          post :create, params: { support_issue: { summary: summary, rich_text_body: body, addressed: 'true' } }
        end.to change(SupportIssue, :count).by(1).and change { ActionMailer::Base.deliveries.size }.by(2)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(help_path)
        expect(flash[:notice]).to match(/Support form successfully submitted./)
        expect(ActionMailer::Base.deliveries.last.subject).to eq(SiteSetting.site_name + ' ' + SupportIssue.last.display_name + ': ' + SupportIssue.last.summary)
        expect(SupportIssue.last.addressed).to be false
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should be successful with valid data' do
        SiteSetting.first.update(email_contact_form_to_admins: 'true')
        SiteSetting.first.update(email_issue_status_to_users: 'true')
        expect do
          post :create, params: { support_issue: { summary: summary, rich_text_body: body } }
        end.to change(SupportIssue, :count).by(1).and change { ActionMailer::Base.deliveries.size }.by(2)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(help_path)
        expect(flash[:notice]).to match(/Support form successfully submitted./)
        expect(SupportIssue.last.addressed).to be false
        expect(SupportIssue.last.user_email).to eq(@user1.email)
        expect(SupportIssue.last.user_id).to eq(@user1.id)
        expect(SupportIssue.last.status_log).to include("Issue Created at")
      end

      it 'should not be successful with invalid data' do
        long_summary = 'a' * 256
        expect do
          post :create, params: { support_issue: { summary: long_summary, body: body } }
        end.to change(SupportIssue, :count).by(0).and change { ActionMailer::Base.deliveries.size }.by(0)
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'should not send admin mailer if email_admins_contact_form? is false' do
        SiteSetting.first.update(email_contact_form_to_admins: 'false')
        SiteSetting.first.update(email_issue_status_to_users: 'true')
        expect do
          post :create, params: { support_issue: { summary: summary, rich_text_body: body } }
        end.to change(SupportIssue, :count).by(1).and change { ActionMailer::Base.deliveries.size }.by(1)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(help_path)
        expect(ActionMailer::Base.deliveries.last.body).to render_template('user_mailer/support_issue_created')
        expect(ActionMailer::Base.deliveries.last.subject).to eq(SiteSetting.site_name + ' ' + SupportIssue.last.display_name + ': ' + SupportIssue.last.summary)
      end

      it 'should not send user mailer if email_issue_status_to_users? is false' do
        SiteSetting.first.update(email_contact_form_to_admins: 'true')
        SiteSetting.first.update(email_issue_status_to_users: 'false')
        expect do
          post :create, params: { support_issue: { summary: summary, rich_text_body: body } }
        end.to change(SupportIssue, :count).by(1).and change { ActionMailer::Base.deliveries.size }.by(1)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(help_path)
        expect(ActionMailer::Base.deliveries.last.body).to render_template('admins_mailer/support_issue_created')
        expect(ActionMailer::Base.deliveries.last.subject).to eq(SiteSetting.site_name + ' ' + SupportIssue.last.display_name + ': ' + SupportIssue.last.summary)
      end

      it 'does not updated addressed field' do
        expect do
          post :create, params: { support_issue: { summary: summary, rich_text_body: body, addressed: 'true' } }
        end.to change(SupportIssue, :count).by(1).and change { ActionMailer::Base.deliveries.size }.by(2)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(help_path)
        expect(flash[:notice]).to match(/Support form successfully submitted./)
        expect(ActionMailer::Base.deliveries.last.subject).to eq(SiteSetting.site_name + ' ' + SupportIssue.last.display_name + ': ' + SupportIssue.last.summary)
        expect(SupportIssue.last.addressed).to be false
      end
    end

    context 'when not logged in' do

      it 'should not be successful' do
        expect do
          post :create, params: { support_issue: { summary: summary, body: body, addressed: 'true' } }
        end.to change(SupportIssue, :count).by(0).and change { ActionMailer::Base.deliveries.size }.by(0)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end

  describe 'PATCH update' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should only update addressed field' do
        SiteSetting.first.update(email_contact_form_to_admins: 'true')
        SiteSetting.first.update(email_issue_status_to_users: 'true')
        support_issue = FactoryBot.create(:support_issue, :rich_text)
        expect(support_issue.addressed).to be false
        expect do
          patch :update, params: { id: support_issue.id, support_issue: { summary: 'updated', body: 'updated', addressed: 'true' } }
        end.to change(SupportIssue, :count).by(0).and change { ActionMailer::Base.deliveries.size }.by(2)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(support_issue)
        expect(flash[:notice]).to match(/Support issue status has been updated./)
        expect(support_issue.reload.addressed).to be true
        expect(support_issue.reload.summary).to_not eq('updated')
        expect(support_issue.reload.body).to_not eq('updated')
        expect(support_issue.reload.status_log).to include("Issue Resolved at")
      end

      it 'should not send admin mailer when email_admins_contact_form is false' do
        SiteSetting.first.update(email_contact_form_to_admins: 'false')
        SiteSetting.first.update(email_issue_status_to_users: 'true')
        support_issue = FactoryBot.create(:support_issue, :rich_text)
        expect(support_issue.addressed).to be false
        expect do
          patch :update, params: { id: support_issue.id, support_issue: { summary: 'updated', body: 'updated', addressed: 'true' } }
        end.to change(SupportIssue, :count).by(0).and change { ActionMailer::Base.deliveries.size }.by(1)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(support_issue)
        expect(ActionMailer::Base.deliveries.last.body).to render_template('user_mailer/support_issue_resolved')
        expect(ActionMailer::Base.deliveries.last.subject).to eq(SiteSetting.site_name + ' ' + SupportIssue.last.display_name + ': ' + SupportIssue.last.summary)
      end

      it 'should not send user mailer when email_issue_status_to_users is false' do
        SiteSetting.first.update(email_contact_form_to_admins: 'true')
        SiteSetting.first.update(email_issue_status_to_users: 'false')
        support_issue = FactoryBot.create(:support_issue, :rich_text)
        expect(support_issue.addressed).to be false
        expect do
          patch :update, params: { id: support_issue.id, support_issue: { summary: 'updated', body: 'updated', addressed: 'true' } }
        end.to change(SupportIssue, :count).by(0).and change { ActionMailer::Base.deliveries.size }.by(1)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(support_issue)
        expect(ActionMailer::Base.deliveries.last.body).to render_template('admins_mailer/support_issue_resolved')
        expect(ActionMailer::Base.deliveries.last.subject).to eq(SiteSetting.site_name + ' ' + SupportIssue.last.display_name + ': ' + SupportIssue.last.summary)
      end

      it 'should send reopened mailers if addressed is updated to false' do
        SiteSetting.first.update(email_contact_form_to_admins: 'true')
        SiteSetting.first.update(email_issue_status_to_users: 'true')
        support_issue = FactoryBot.create(:support_issue, :rich_text)
        expect(support_issue.addressed).to be false
        expect do
          patch :update, params: { id: support_issue.id, support_issue: { summary: 'updated', body: 'updated', addressed: 'false' } }
        end.to change(SupportIssue, :count).by(0).and change { ActionMailer::Base.deliveries.size }.by(2)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(support_issue)
        expect(ActionMailer::Base.deliveries.last.body).to render_template('admins_mailer/support_issue_reopened')
        expect(ActionMailer::Base.deliveries[-1].body).to render_template('user_mailer/support_issue_reopened')
        expect(support_issue.reload.status_log).to include("Issue Reopened at")
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful' do
        support_issue = FactoryBot.create(:support_issue, :rich_text, user_id: @user1.id)
        expect do
          patch :update, params: { id: support_issue.id, support_issue: { summary: 'updated', body: 'updated', addressed: 'true' } }
        end.to change(SupportIssue, :count).by(0).and change { ActionMailer::Base.deliveries.size }.by(0)
        expect(response).to redirect_to(root_path)
        expect(support_issue.reload.addressed).to be false
        expect(support_issue.reload.summary).to_not eq('updated')
        expect(support_issue.reload.body).to_not eq('updated')
      end
    end

    context 'when not logged in' do

      it 'should not be successful' do
        support_issue = FactoryBot.create(:support_issue, :rich_text)
        expect do
          patch :update, params: { id: support_issue.id, support_issue: { summary: 'updated', body: 'updated', addressed: 'true' } }
        end.to change(SupportIssue, :count).by(0).and change { ActionMailer::Base.deliveries.size }.by(0)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
        expect(support_issue.reload.addressed).to be false
        expect(support_issue.reload.summary).to_not eq('updated')
        expect(support_issue.reload.body).to_not eq('updated')
      end
    end
  end

  describe 'PUT update' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should only update addressed field' do
        SiteSetting.first.update(email_contact_form_to_admins: 'true')
        SiteSetting.first.update(email_issue_status_to_users: 'true')
        support_issue = FactoryBot.create(:support_issue, :rich_text)
        expect(support_issue.addressed).to be false
        expect do
          put :update, params: { id: support_issue.id, support_issue: { summary: 'updated', body: 'updated', addressed: 'true' } }
        end.to change(SupportIssue, :count).by(0).and change { ActionMailer::Base.deliveries.size }.by(2)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(support_issue)
        expect(flash[:notice]).to match(/Support issue status has been updated./)
        expect(support_issue.reload.addressed).to be true
        expect(support_issue.reload.summary).to_not eq('updated')
        expect(support_issue.reload.body).to_not eq('updated')
      end

      it 'should not send admin mailer when email_admins_contact_form is false' do
        SiteSetting.first.update(email_contact_form_to_admins: 'false')
        SiteSetting.first.update(email_issue_status_to_users: 'true')
        support_issue = FactoryBot.create(:support_issue, :rich_text)
        expect(support_issue.addressed).to be false
        expect do
          put :update, params: { id: support_issue.id, support_issue: { summary: 'updated', body: 'updated', addressed: 'true' } }
        end.to change(SupportIssue, :count).by(0).and change { ActionMailer::Base.deliveries.size }.by(1)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(support_issue)
        expect(ActionMailer::Base.deliveries.last.body).to render_template('user_mailer/support_issue_resolved')
        expect(ActionMailer::Base.deliveries.last.subject).to eq(SiteSetting.site_name + ' ' + SupportIssue.last.display_name + ': ' + SupportIssue.last.summary)
      end

      it 'should not send user mailer when email_issue_status_to_users is false' do
        SiteSetting.first.update(email_contact_form_to_admins: 'true')
        SiteSetting.first.update(email_issue_status_to_users: 'false')
        support_issue = FactoryBot.create(:support_issue, :rich_text)
        expect(support_issue.addressed).to be false
        expect do
          put :update, params: { id: support_issue.id, support_issue: { summary: 'updated', body: 'updated', addressed: 'true' } }
        end.to change(SupportIssue, :count).by(0).and change { ActionMailer::Base.deliveries.size }.by(1)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(support_issue)
        expect(ActionMailer::Base.deliveries.last.body).to render_template('admins_mailer/support_issue_resolved')
        expect(ActionMailer::Base.deliveries.last.subject).to eq(SiteSetting.site_name + ' ' + SupportIssue.last.display_name + ': ' + SupportIssue.last.summary)
      end

      it 'should send reopened mailers if addressed is updated to false' do
        SiteSetting.first.update(email_contact_form_to_admins: 'true')
        SiteSetting.first.update(email_issue_status_to_users: 'true')
        support_issue = FactoryBot.create(:support_issue, :rich_text)
        expect(support_issue.addressed).to be false
        expect do
          put :update, params: { id: support_issue.id, support_issue: { summary: 'updated', body: 'updated', addressed: 'false' } }
        end.to change(SupportIssue, :count).by(0).and change { ActionMailer::Base.deliveries.size }.by(2)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(support_issue)
        expect(ActionMailer::Base.deliveries.last.body).to render_template('admins_mailer/support_issue_reopened')
        expect(ActionMailer::Base.deliveries[-1].body).to render_template('user_mailer/support_issue_reopened')
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should not be successful' do
        support_issue = FactoryBot.create(:support_issue, :rich_text, user_id: @user1.id)
        expect do
          put :update, params: { id: support_issue.id, support_issue: { summary: 'updated', body: 'updated', addressed: 'true' } }
        end.to change(SupportIssue, :count).by(0).and change { ActionMailer::Base.deliveries.size }.by(0)
        expect(response).to redirect_to(root_path)
        expect(support_issue.reload.addressed).to be false
        expect(support_issue.reload.summary).to_not eq('updated')
        expect(support_issue.reload.body).to_not eq('updated')
      end
    end

    context 'when not logged in' do

      it 'should not be successful' do
        support_issue = FactoryBot.create(:support_issue, :rich_text)
        expect do
          put :update, params: { id: support_issue.id, support_issue: { summary: 'updated', body: 'updated', addressed: 'true' } }
        end.to change(SupportIssue, :count).by(0).and change { ActionMailer::Base.deliveries.size }.by(0)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
        expect(support_issue.reload.addressed).to be false
        expect(support_issue.reload.summary).to_not eq('updated')
        expect(support_issue.reload.body).to_not eq('updated')
      end
    end
  end
end
