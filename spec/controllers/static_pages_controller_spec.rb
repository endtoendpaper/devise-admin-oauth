require 'rails_helper'

RSpec.describe StaticPagesController, type: :controller do
  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @user = FactoryBot.create(:user, :confirmed)
  end

  describe 'GET home' do
    it 'returns a 200 if not logged in' do
      get :home
      expect(response).to have_http_status(:ok)
    end

    it 'returns a 200 if logged in as user' do
      sign_in @user
      get :home
      expect(response).to have_http_status(:ok)
    end

    it 'returns a 200 if logged in as admin' do
      sign_in @admin
      get :home
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'GET about' do
    it 'returns a 200 if not logged in' do
      get :about
      expect(response).to have_http_status(:ok)
    end

    it 'returns a 200 if logged in as user' do
      sign_in @user
      get :about
      expect(response).to have_http_status(:ok)
    end

    it 'returns a 200 if logged in as admin' do
      sign_in @admin
      get :about
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'GET help' do
    it 'returns a 302 found if not logged in' do
      get :help
      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(new_user_session_path)
      expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
    end

    it 'returns a 200 if logged in as user' do
      sign_in @user
      get :help
      expect(response).to have_http_status(:ok)
    end

    it 'returns a 200 if logged in as admin' do
      sign_in @admin
      get :help
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'GET user_management' do
    it 'returns a 302 found if not logged in' do
      get :user_management
      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(new_user_session_path)
      expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
    end

    it 'returns a 200 if logged in as user' do
      sign_in @user
      get :user_management
      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(root_path)
    end

    it 'returns a 200 if logged in as admin' do
      sign_in @admin
      get :user_management
      expect(response).to have_http_status(:ok)
    end
  end
end
