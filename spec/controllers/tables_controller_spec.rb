require 'rails_helper'

RSpec.describe TablesController, type: :controller do
  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @user = FactoryBot.create(:user, :confirmed)
    @help_section = FactoryBot.create(:help_section)
    @table = FactoryBot.create(:table, :record_saved, record_id: @help_section.id, record_type: 'HelpSection')
  end

  shared_context 'when admin is signed in' do
    before(:each) do
      sign_in @admin
    end
  end

  shared_context 'when user is signed in' do
    before(:each) do
      sign_in @user
    end
  end

  describe 'GET show' do

    context 'when admin is signed in' do
      include_context 'when admin is signed in'

      it 'is successful' do
        get :show, params: { id: @table.attachable_sgid }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when user is signed in' do
      include_context 'when user is signed in'

      it 'is not successful' do
        get :show, params: { id: @table.attachable_sgid }
        expect(response).to have_http_status(:found)
      end
    end

    context 'when not logged in' do

      it 'is not successful' do
        get :show, params: { id: @table.attachable_sgid }
        expect(response).to have_http_status(:found)
      end
    end
  end

  describe 'POST create' do

    context 'when admin is signed in' do
      include_context 'when admin is signed in'

      it 'is successful' do
        post :create
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when user is signed in' do
      include_context 'when user is signed in'

      it 'is successful if it is a new record' do
        post :create
        expect(response).to have_http_status(:ok)
      end

      it 'is not successful if record is already created' do
        post :create,
        params: { record_id: @help_section.id, record_type: 'HelpSection' }
        expect(response).to have_http_status(:found)
      end
    end

    context 'when not logged in' do

      it 'is not successful' do
        post :create
        expect(response).to have_http_status(:found)
      end
    end
  end

  describe 'PATCH update' do

    context 'for admin' do
      include_context 'when admin is signed in'

      it 'is successful' do
        patch :update,
        params: { id: @table.attachable_sgid }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'for user' do
      include_context 'when user is signed in'

      it 'is successful if it is a new record' do
        table = FactoryBot.create(:table)
        patch :update,
        params: { id: table.attachable_sgid }
        expect(response).to have_http_status(:ok)
      end

      it 'is not successful if record is already created' do
        patch :update,
        params: { id: @table.attachable_sgid }
        expect(response).to have_http_status(:found)
      end
    end

    context 'when not logged in' do

      it 'is not successful' do
        patch :update,
        params: { id: @table.attachable_sgid }
        expect(response).to have_http_status(:found)
      end
    end
  end
end
