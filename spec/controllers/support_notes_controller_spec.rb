require 'rails_helper'

RSpec.describe SupportNotesController, type: :controller do

  let(:body) { Faker::Lorem.sentence(word_count: 200) }
  let(:invalid_body) { Faker::Lorem.sentence(word_count: 2000) }

  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @admin2 = FactoryBot.create(:user, :confirmed, :admin)
    @user1 = FactoryBot.create(:user, :confirmed)
    @user2 = FactoryBot.create(:user, :confirmed)
    @support_issue1 = FactoryBot.create(:support_issue, :rich_text, user_id: @user1.id)
    @support_issue2 = FactoryBot.create(:support_issue, :rich_text, user_id: @admin.id)
  end

  shared_context 'when admin logged in' do
    before(:each) do
      sign_in @admin
    end
  end

  shared_context 'when user logged in' do
    before(:each) do
      sign_in @user1
    end
  end

  describe 'POST create' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful with valid data if admin did not create issue' do
        SiteSetting.first.update(rich_text_support_replies: 'true')
        expect do
          post :create, params: { support_issue_id: @support_issue1.id, support_note: { rich_text_body: body } }
        end.to change(SupportNote, :count).by(1).and change { ActionMailer::Base.deliveries.size }.by(1)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(support_issue_path(@support_issue1))
        expect(flash[:notice]).to match(/Internal note successfully submitted./)
        expect(SupportNote.last.user_email).to eq(@admin.email)
        expect(SupportNote.last.user_id).to eq(@admin.id)
        expect(ActionMailer::Base.deliveries.last.subject).to eq(SiteSetting.site_name + ' ' + @support_issue1.display_name + ': ' + @support_issue1.summary)
        expect(ActionMailer::Base.deliveries.last.body).to render_template('admins_mailer/support_note')
      end

      it 'should be unsuccessful with valid data if admin created issue' do
        SiteSetting.first.update(rich_text_support_replies: 'true')
        expect do
          post :create, params: { support_issue_id: @support_issue2.id, support_note: { rich_text_body: body } }
        end.to change(SupportNote, :count).by(0).and change { ActionMailer::Base.deliveries.size }.by(0)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
      end

      it 'should be unsuccessful with invalid data if admin did not create issue' do
        SiteSetting.first.update(rich_text_support_replies: 'false')
        expect do
          post :create, params: { support_issue_id: @support_issue1.id, support_note: { body: invalid_body } }
        end.to change(SupportNote, :count).by(0).and change { ActionMailer::Base.deliveries.size }.by(0)
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'should be unsuccessful with rich text if rich_text_support_issues = false' do
        SiteSetting.first.update(rich_text_support_replies: 'false')
        expect do
          post :create, params: { support_issue_id: @support_issue1.id, support_note: { rich_text_body: body } }
        end.to change(SupportNote, :count).by(0).and change { ActionMailer::Base.deliveries.size }.by(0)
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should be unsuccessful' do
        SiteSetting.first.update(rich_text_support_replies: 'true')
        expect do
          post :create, params: { support_issue_id: @support_issue1.id, support_note: { rich_text_body: body } }
        end.to change(SupportNote, :count).by(0).and change { ActionMailer::Base.deliveries.size }.by(0)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not logged in' do

      it 'should be unsuccessful' do
        expect do
          post :create, params: { support_issue_id: @support_issue1.id, support_reply: { rich_text_body: body } }
        end.to change(SupportReply, :count).by(0).and change { ActionMailer::Base.deliveries.size }.by(0)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end
end
