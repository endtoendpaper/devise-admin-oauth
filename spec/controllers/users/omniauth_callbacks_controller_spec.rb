require 'rails_helper'

RSpec.describe Users::OmniauthCallbacksController, type: :controller do
  before(:each) do
    @siteSettings = SiteSetting.first
    @request.env['devise.mapping'] = Devise.mappings[:user]
    OmniAuth.config.mock_auth[:google_oauth2] = OmniAuth::AuthHash.new({
        provider: 'google_oauth2',
        uid: '12345678910',
        info: {
          email: 'tstark@gmail.com',
          first_name: 'Tony',
          last_name: 'Stark'
        },
        credentials: {
          token: 'abcdefg12345',
          refresh_token: 'abcdefg12345',
          expires_at: Time.current
        }
      })
    @request.env['omniauth.auth'] = OmniAuth.config.mock_auth[:google_oauth2]
  end

  describe 'POST google_oauth2' do
    it 'does not create new user when users_can_join = false' do
      @siteSettings.update(users_can_join: false)
      expect do
        post :google_oauth2
      end.to change(User, :count).by(0).and change(Devise.mailer.deliveries, :count).by(0)
      expect(cookies[:remember_user_token]).to be_nil
      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(new_user_session_path)
    end

    it 'creates and signs in new user when users_can_join = true' do
      user = User.create!(username: 'tstark', admin: true,
        email: 'something@gmail.com', password: 'foobar',
        password_confirmation: 'foobar')
      user = User.create!(username: 'tstark1', admin: true,
        email: 'something_else@gmail.com', password: 'foobar',
        password_confirmation: 'foobar')
      @siteSettings.update(users_can_join: true)
      expect do
        post :google_oauth2
      end.to change(User, :count).by(1).and change(Devise.mailer.deliveries, :count).by(0)
      expect(User.ordered_by_id.last.username).to eq('tstark2')
      expect(User.ordered_by_id.last.current_sign_in_at).to_not be_nil
      expect(cookies[:remember_user_token]).to_not be_nil
      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(root_path)
      expect(flash[:alert]).to be_nil
    end

    it 'logs in confirmed user' do
      user = User.create!(username: 'tonys', admin: true,
        email: 'tstark@gmail.com', password: 'foobar',
        password_confirmation: 'foobar')
      user.skip_confirmation!
      user.save!
      expect do
        post :google_oauth2
      end.to change(User, :count).by(0).and change(Devise.mailer.deliveries, :count).by(0)
      expect(User.ordered_by_id.last.username).to eq('tonys')
      expect(User.ordered_by_id.last.current_sign_in_at).to_not be_nil
      expect(cookies[:remember_user_token]).to_not be_nil
      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(root_path)
      expect(flash[:alert]).to be_nil
    end
  end
end
