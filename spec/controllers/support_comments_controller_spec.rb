require 'rails_helper'

RSpec.describe SupportCommentsController, type: :controller do

  let(:body) { Faker::Lorem.sentence(word_count: 200) }

  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @admin2 = FactoryBot.create(:user, :confirmed, :admin)
    @user1 = FactoryBot.create(:user, :confirmed)
    @user2 = FactoryBot.create(:user, :confirmed)
    @support_issue1 = FactoryBot.create(:support_issue, :rich_text, user_id: @user1.id)
    @support_issue2 = FactoryBot.create(:support_issue, :rich_text, user_id: @admin.id)
  end

  shared_context 'when admin logged in' do
    before(:each) do
      sign_in @admin
    end
  end

  shared_context 'when user logged in' do
    before(:each) do
      sign_in @user1
    end
  end

  describe 'POST create' do
    context 'for admin user' do
      include_context 'when admin logged in'

      it 'should be successful with valid data if admin created issue' do
        SiteSetting.first.update(rich_text_support_replies: 'true')
        expect do
          post :create, params: { support_issue_id: @support_issue2.id, support_comment: { rich_text_body: body } }
        end.to change(SupportComment, :count).by(1).and change { ActionMailer::Base.deliveries.size }.by(1)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(support_issue_path(@support_issue2))
        expect(flash[:notice]).to match(/Comment successfully submitted./)
        expect(ActionMailer::Base.deliveries.last.subject).to eq(SiteSetting.site_name + ' ' + @support_issue2.display_name + ': ' + @support_issue2.summary)
        expect(SupportComment.last.user_email).to eq(@admin.email)
        expect(SupportComment.last.user_id).to eq(@admin.id)
        expect(ActionMailer::Base.deliveries.last.body).to render_template('admins_mailer/support_comment')
      end

      it 'should not send mailers if email_admins_contact_form? is false' do
        SiteSetting.first.update(rich_text_support_issues: 'false')
        SiteSetting.first.update(email_contact_form_to_admins: 'false')
        expect do
          post :create, params: { support_issue_id: @support_issue2.id, support_comment: { body: body } }
        end.to change(SupportComment, :count).by(1).and change { ActionMailer::Base.deliveries.size }.by(0)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(support_issue_path(@support_issue2))
        expect(flash[:notice]).to match(/Comment successfully submitted./)
        expect(SupportComment.last.user_email).to eq(@admin.email)
        expect(SupportComment.last.user_id).to eq(@admin.id)
      end

      it 'should be unsuccessful with invalid data if admin did create issue' do
        SiteSetting.first.update(rich_text_support_replies: 'false')
        long_body = 'a' * 2001
        expect do
          post :create, params: { support_issue_id: @support_issue2.id, support_comment: { body: long_body } }
        end.to change(SupportComment, :count).by(0).and change { ActionMailer::Base.deliveries.size }.by(0)
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'should not be successful with valid data if admin did not create issue' do
        SiteSetting.first.update(email_contact_form_to_admins: 'true')
        SiteSetting.first.update(rich_text_support_replies: 'true')
        expect do
          post :create, params: { support_issue_id: @support_issue1.id, support_comment: { rich_text_body: body } }
        end.to change(SupportComment, :count).by(0).and change { ActionMailer::Base.deliveries.size }.by(0)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
      end

      it 'should be unsuccessful if admin created issue and issue is resolved' do
        SiteSetting.first.update(rich_text_support_replies: 'true')
        @support_issue2.update(addressed: true)
        expect do
          post :create, params: { support_issue_id: @support_issue2.id, support_comment: { rich_text_body: body } }
        end.to change(SupportComment, :count).by(0).and change { ActionMailer::Base.deliveries.size }.by(0)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(support_issue_path(@support_issue2))
        expect(flash[:alert]).to match(/Issue has already been resolved./)
      end
    end

    context 'for user' do
      include_context 'when user logged in'

      it 'should be successful with valid data if user created issue' do
        SiteSetting.first.update(rich_text_support_replies: 'true')
        expect do
          post :create, params: { support_issue_id: @support_issue1.id, support_comment: { rich_text_body: body } }
        end.to change(SupportComment, :count).by(1).and change { ActionMailer::Base.deliveries.size }.by(1)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(support_issue_path(@support_issue1))
        expect(flash[:notice]).to match(/Comment successfully submitted./)
        expect(ActionMailer::Base.deliveries.last.subject).to eq(SiteSetting.site_name + ' ' + @support_issue1.display_name + ': ' + @support_issue1.summary)
        expect(SupportComment.last.user_email).to eq(@user1.email)
        expect(SupportComment.last.user_id).to eq(@user1.id)
        expect(ActionMailer::Base.deliveries.last.body).to render_template('admins_mailer/support_comment')
      end

      it 'should not send mailers if email_admins_contact_form? is false' do
        SiteSetting.first.update(rich_text_support_issues: 'false')
        SiteSetting.first.update(email_contact_form_to_admins: 'false')
        expect do
          post :create, params: { support_issue_id: @support_issue1.id, support_comment: { body: body } }
        end.to change(SupportComment, :count).by(1).and change { ActionMailer::Base.deliveries.size }.by(0)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(support_issue_path(@support_issue1))
        expect(flash[:notice]).to match(/Comment successfully submitted./)
        expect(SupportComment.last.user_email).to eq(@user1.email)
        expect(SupportComment.last.user_id).to eq(@user1.id)
      end

      it 'should be unsuccessful with invalid data if user did create issue' do
        SiteSetting.first.update(rich_text_support_replies: 'false')
        long_body = 'a' * 2001
        expect do
          post :create, params: { support_issue_id: @support_issue1.id, support_comment: { body: long_body } }
        end.to change(SupportComment, :count).by(0).and change { ActionMailer::Base.deliveries.size }.by(0)
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'should not be successful with valid data if user did not create issue' do
        SiteSetting.first.update(email_contact_form_to_admins: 'true')
        SiteSetting.first.update(rich_text_support_replies: 'true')
        expect do
          post :create, params: { support_issue_id: @support_issue2.id, support_comment: { rich_text_body: body } }
        end.to change(SupportComment, :count).by(0).and change { ActionMailer::Base.deliveries.size }.by(0)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
      end

      it 'should be unsuccessful if user created issue and issue is resolved' do
        SiteSetting.first.update(rich_text_support_replies: 'true')
        @support_issue1.update(addressed: true)
        expect do
          post :create, params: { support_issue_id: @support_issue1.id, support_comment: { rich_text_body: body } }
        end.to change(SupportComment, :count).by(0).and change { ActionMailer::Base.deliveries.size }.by(0)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(support_issue_path(@support_issue1))
        expect(flash[:alert]).to match(/Issue has already been resolved./)
      end
    end

    context 'when not logged in' do

      it 'should be unsuccessful' do
        expect do
          post :create, params: { support_issue_id: @support_issue1.id, support_reply: { rich_text_body: body } }
        end.to change(SupportReply, :count).by(0).and change { ActionMailer::Base.deliveries.size }.by(0)
        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to match(/You need to sign in or sign up before continuing./)
      end
    end
  end
end
