require 'rails_helper'

RSpec.describe Relationship, type: :model do
  before(:each) do
    @relationship = FactoryBot.create(:relationship)
  end

  it 'is valid with valid data' do
    expect(@relationship).to be_valid
  end

  it "requires a follower_id" do
    @relationship.follower_id = nil
    expect(@relationship).to_not be_valid
  end

  it "requires a followed_id" do
    @relationship.followed_id = nil
    expect(@relationship).to_not be_valid
  end
end
