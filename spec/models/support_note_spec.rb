require 'rails_helper'

RSpec.describe SupportNote, type: :model do
  it 'is valid with valid data in plain text' do
    SiteSetting.first.update(rich_text_support_replies: 'false')
    support_issue = FactoryBot.create(:support_issue, :rich_text)
    note = SupportNote.new
    note.user_id = FactoryBot.create(:user, :confirmed).id
    note.support_issue_id = support_issue.id
    note.body = Faker::Lorem.sentences(number: 20)
    expect(note).to be_valid
  end

  it 'is valid with valid data in rich text' do
    support_issue = FactoryBot.create(:support_issue, :rich_text)
    note = SupportReply.new
    note.user_id = FactoryBot.create(:user, :confirmed).id
    note.support_issue_id = support_issue.id
    note.rich_text_body = Faker::Lorem.sentences(number: 20)
    expect(note).to be_valid
  end

  it 'is invalid without either a body or a rich text body' do
    support_issue = FactoryBot.create(:support_issue, :rich_text)
    note = SupportReply.new
    note.support_issue_id = support_issue.id
    expect(note).to_not be_valid
  end

  it 'is invalid with both a body and a rich text body' do
    support_issue = FactoryBot.create(:support_issue, :rich_text)
    reply = SupportReply.new
    reply.support_issue_id = support_issue.id
    reply.rich_text_body = Faker::Lorem.sentences(number: 20)
    reply.body = Faker::Lorem.sentences(number: 20)
    expect(reply).to_not be_valid
  end

  it 'is valid without a user' do
    SiteSetting.first.update(rich_text_support_replies: 'false')
    support_issue = FactoryBot.create(:support_issue, :rich_text)
    note = SupportReply.new
    note.support_issue_id = support_issue.id
    note.body = Faker::Lorem.sentences(number: 20)
    expect(note).to be_valid
  end

  it 'is invalid with a body less than 10 char' do
    support_issue = FactoryBot.create(:support_issue, :rich_text)
    note = SupportReply.new
    note.support_issue_id = support_issue.id
    note.body = 'a' * 9
    expect(note).to_not be_valid
  end

  it 'is invalid with a body more than 2000 char' do
    support_issue = FactoryBot.create(:support_issue, :rich_text)
    note = SupportReply.new
    note.support_issue_id = support_issue.id
    note.body = 'a' * 2001
    expect(note).to_not be_valid
  end

  it 'is invalid with a rich_text_body more than 2000 char' do
    support_issue = FactoryBot.create(:support_issue, :rich_text)
    note = SupportReply.new
    note.support_issue_id = support_issue.id
    note.rich_text_body = 'a' * 2001
    expect(note).to_not be_valid
  end

  it 'is invalid with rich_text_body when rich_text_support_issues = false' do
    SiteSetting.first.update(rich_text_support_issues: false)
    note = SupportNote.new
    note.user_id = FactoryBot.create(:user, :confirmed).id
    note.rich_text_body = Faker::Lorem.sentences(number: 20)
    expect(note).to_not be_valid
  end
end
