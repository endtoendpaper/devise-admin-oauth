require 'rails_helper'

RSpec.describe SupportComment, type: :model do
  it 'is valid with valid data in plain text' do
    SiteSetting.first.update(rich_text_support_replies: 'false')
    support_issue = FactoryBot.create(:support_issue, :rich_text)
    comment = SupportNote.new
    comment.user_id = FactoryBot.create(:user, :confirmed).id
    comment.support_issue_id = support_issue.id
    comment.body = Faker::Lorem.sentences(number: 20)
    expect(comment).to be_valid
  end

  it 'is valid with valid data in rich text' do
    support_issue = FactoryBot.create(:support_issue, :rich_text)
    comment = SupportReply.new
    comment.user_id = FactoryBot.create(:user, :confirmed).id
    comment.support_issue_id = support_issue.id
    comment.rich_text_body = Faker::Lorem.sentences(number: 20)
    expect(comment).to be_valid
  end

  it 'is invalid without either a body or a rich text body' do
    support_issue = FactoryBot.create(:support_issue, :rich_text)
    comment = SupportReply.new
    comment.support_issue_id = support_issue.id
    expect(comment).to_not be_valid
  end

  it 'is invalid with both a body and a rich text body' do
    support_issue = FactoryBot.create(:support_issue, :rich_text)
    comment = SupportReply.new
    comment.support_issue_id = support_issue.id
    comment.rich_text_body = Faker::Lorem.sentences(number: 20)
    comment.body = Faker::Lorem.sentences(number: 20)
    expect(comment).to_not be_valid
  end

  it 'is valid without a user' do
    SiteSetting.first.update(rich_text_support_replies: 'false')
    support_issue = FactoryBot.create(:support_issue, :rich_text)
    comment = SupportReply.new
    comment.support_issue_id = support_issue.id
    comment.body = Faker::Lorem.sentences(number: 20)
    expect(comment).to be_valid
  end

  it 'is invalid with a body less than 10 char' do
    support_issue = FactoryBot.create(:support_issue, :rich_text)
    comment = SupportReply.new
    comment.support_issue_id = support_issue.id
    comment.body = 'a' * 9
    expect(comment).to_not be_valid
  end

  it 'is invalid with a body more than 2000 char' do
    support_issue = FactoryBot.create(:support_issue, :rich_text)
    comment = SupportReply.new
    comment.support_issue_id = support_issue.id
    comment.body = 'a' * 2001
    expect(comment).to_not be_valid
  end

  it 'is invalid with a rich_text_body more than 2000 char' do
    support_issue = FactoryBot.create(:support_issue, :rich_text)
    comment = SupportReply.new
    comment.support_issue_id = support_issue.id
    comment.rich_text_body = 'a' * 2001
    expect(comment).to_not be_valid
  end

  it 'is invalid with rich_text_body when rich_text_support_issues = false' do
    SiteSetting.first.update(rich_text_support_issues: false)
    comment = SupportComment.new
    comment.user_id = FactoryBot.create(:user, :confirmed).id
    comment.rich_text_body = Faker::Lorem.sentences(number: 20)
    expect(comment).to_not be_valid
  end
end
