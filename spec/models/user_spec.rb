require 'rails_helper'

RSpec.describe User, type: :model do
  before(:each) do
    @jpg_file = Rack::Test::UploadedFile
                .new('spec/factories/images/default-avatar.jpg', 'image/jpeg')
    @pdf_file = Rack::Test::UploadedFile
                .new('spec/factories/files/test.pdf')
    @txt_file = Rack::Test::UploadedFile
                .new('spec/factories/files/test.txt')
    @png_file = Rack::Test::UploadedFile
                .new('spec/factories/images/rails-logo.png', 'image/jpeg')
  end

  it 'is valid with valid attributes' do
    user = User.new(email: 'test@test', username: 'myuser',
                    password: 'mypwd123', password_confirmation: 'mypwd123')
    expect(user).to be_valid
  end

  it 'is not valid without a password' do
    user = User.new(email: 'test2@test', username: 'myuser2')
    expect(user).to_not be_valid
  end

  it 'is not valid without email' do
    user = User.new(username: 'myuser3', password: 'mypwd123',
                    password_confirmation: 'mypwd123')
    expect(user).to_not be_valid
  end

  it 'is not valid without username' do
    user = User.new(email: 'test4@test', password: 'mypwd123',
                    password_confirmation: 'mypwd123')
    expect(user).to_not be_valid
  end

  it 'is not valid with a username > 100 char' do
    uname = 'a' * 101
    user = User.new(email: 'test5@test',
      username: uname,
      password: 'mypwd123', password_confirmation: 'mypwd123')
    expect(user).to_not be_valid
  end

  it 'is not valid with spaces in the username' do
    user = User.new(email: 'test6@test', username: '32gfj mcy1',
                    password: 'mypwd123', password_confirmation: 'mypwd123')
    expect(user).to_not be_valid
  end

  it 'is not valid with @ char in the username' do
    user = User.new(email: 'test6@test', username: '32gfj@mcy1',
                    password: 'mypwd123', password_confirmation: 'mypwd123')
    expect(user).to_not be_valid
  end

  it 'validates uniqueness of username' do
    user1 = User.create(email: 'test7@test', username: 'myuser7',
                     password: 'mypwd123', password_confirmation: 'mypwd123')
    user2 = User.new(email: 'test8@test', username: 'myuser7',
                     password: 'mypwd123', password_confirmation: 'mypwd123')
    expect(user2).to_not be_valid
  end

  it 'validates uniqueness of email' do
    user1 = User.create(email: 'test9@test', username: 'myuser9',
                     password: 'mypwd123', password_confirmation: 'mypwd123')
    user2 = User.new(email: 'test10@test', username: 'myuser9',
                     password: 'mypwd123', password_confirmation: 'mypwd123')
    expect(user2).to_not be_valid
  end

  it "is not valid if password doesn't match password_confirmation" do
    user = User.new(email: 'test11@test', username: 'myuser11',
                    password: 'mypwd123', password_confirmation: 'mypwd12')
    expect(user).to_not be_valid
  end

  it 'has a default value of false for admin' do
    user = User.new(email: 'test12@test', username: 'myuser12',
                    password: 'mypwd123', password_confirmation: 'mypwd123')
    expect(user.admin).to be false
  end

  it 'validates uniqueness of reset_password_token' do
    user1 = User.create(email: 'test7@test', username: 'myuser7',
                     password: 'mypwd123', password_confirmation: 'mypwd123',
                     reset_password_token: '12345')
    user2 = User.new(email: 'test8@test', username: 'myuser8',
                     password: 'mypwd123', password_confirmation: 'mypwd123',
                     reset_password_token: '12345')
    expect { user2.save }.to raise_error(ActiveRecord::RecordNotUnique)
  end

  it 'validates uniqueness of confirmation_token' do
    user1 = User.create(email: 'test7@test', username: 'myuser7',
                     password: 'mypwd123', password_confirmation: 'mypwd123',
                     confirmation_token: '12345')
    user2 = User.new(email: 'test8@test', username: 'myuser8',
                    password: 'mypwd123', password_confirmation: 'mypwd123',
                     confirmation_token: '12345')
    expect { user2.save }.to raise_error(ActiveRecord::RecordNotUnique)
  end

  it 'validates uniqueness of unlock_token' do
    user1 = User.create(email: 'test7@test', username: 'myuser7',
                     password: 'mypwd123', password_confirmation: 'mypwd123',
                     unlock_token: '12345')
    user2 = User.new(email: 'test8@test', username: 'myuser8',
                     password: 'mypwd123', password_confirmation: 'mypwd123',
                     unlock_token: '12345')
    expect { user2.save }.to raise_error(ActiveRecord::RecordNotUnique)
  end

  it 'is valid with jpeg avatar' do
    user1 = User.new(email: 'test7@test', username: 'myuser7',
                     password: 'mypwd123', password_confirmation: 'mypwd123',
                     avatar: @jpg_file)
    expect { user1.save }.to_not raise_error
  end

  it 'is valid with png avatar' do
    user1 = User.new(email: 'test7@test', username: 'myuser7',
                     password: 'mypwd123', password_confirmation: 'mypwd123',
                     avatar: @png_file)
    expect { user1.save }.to_not raise_error
  end

  it 'is not valid with pdf avatar' do
    user1 = User.new(email: 'test7@test', username: 'myuser7',
                     password: 'mypwd123', password_confirmation: 'mypwd123',
                     avatar: @pdf_file)
    expect(user1).to_not be_valid
  end

  it 'is not valid with text avatar' do
    user1 = User.new(email: 'test7@test', username: 'myuser7',
                     password: 'mypwd123', password_confirmation: 'mypwd123',
                     avatar: @txt_file)
    expect(user1).to_not be_valid
  end

  it 'nulifies users support_issues fk if user is destroyed' do
    user = FactoryBot.create(:user)
    FactoryBot.create(:support_issue, :rich_text, user_id: user.id)
    FactoryBot.create(:support_issue, :rich_text, user_id: user.id)
    expect do
      user.destroy
    end.to change(SupportIssue, :count).by(0)
  end

  it 'nulifies users support_replies fk if user is destroyed' do
    user1 = FactoryBot.create(:user)
    user2 = FactoryBot.create(:user)
    support_issue = FactoryBot.create(:support_issue, :rich_text, user_id: user1.id)
    FactoryBot.create(:support_reply, :rich_text, user_id: user2.id, support_issue_id: support_issue.id)
    FactoryBot.create(:support_reply, :rich_text, user_id: user2.id, support_issue_id: support_issue.id)
    expect do
      user2.destroy
    end.to change(SupportReply, :count).by(0)
  end

  it "follows and unfollows a user if follow_users is true" do
    SiteSetting.first.update(follow_users: true)
    user1 = FactoryBot.create(:user)
    user2 = FactoryBot.create(:user)
    expect(user1.following?(user2)).to be false
    user1.follow(user2)
    expect(user1.following?(user2)).to be true
    expect(user2.followers.include?(user1)).to be true
    user1.unfollow(user2)
    expect(user1.following?(user2)).to be false
    # Users can't follow themselves.
    user1.follow(user1)
    expect(user1.following?(user1)).to be false
  end

  it "does not follow and unfolloww a user if follow_users if false" do
    SiteSetting.first.update(follow_users: true)
    user1 = FactoryBot.create(:user)
    user2 = FactoryBot.create(:user)
    user3 = FactoryBot.create(:user)
    expect(user1.following?(user2)).to be false
    user1.follow(user2)
    expect(user1.following?(user2)).to be true
    SiteSetting.first.update(follow_users: false)
    user1.unfollow(user2)
    expect(user2.followers.include?(user1)).to be true
    expect(user1.following?(user3)).to be false
    user1.follow(user3)
    expect(user1.following?(user3)).to be false
  end
end
