require 'rails_helper'

RSpec.describe HelpSection, type: :model do
  it 'is valid with valid data' do
    helpSection = HelpSection.new
    helpSection.title = Faker::Lorem.sentence(word_count: 6)
    helpSection.content = Faker::Lorem.paragraphs(number: 4)
    expect(helpSection).to be_valid
  end

  it 'is not valid without a title' do
    helpSection = HelpSection.new
    helpSection.content = Faker::Lorem.paragraphs(number: 4)
    expect(helpSection).to_not be_valid
  end

  it 'is not valid without content' do
    helpSection = HelpSection.new
    helpSection.title = Faker::Lorem.sentence(word_count: 6)
    expect(helpSection).to_not be_valid
  end

  it 'is not valid with a title greater than 100 char' do
    helpSection = HelpSection.new
    helpSection.title = 'a' * 101
    helpSection.content = Faker::Lorem.paragraphs(number: 4)
    expect(helpSection).to_not be_valid
  end
end
