require 'rails_helper'

RSpec.describe SiteSetting, type: :model do
  before(:each) do
    @jpg_file = Rack::Test::UploadedFile
                .new('spec/factories/images/default-avatar.jpg', 'image/jpeg')
    @pdf_file = Rack::Test::UploadedFile
                .new('spec/factories/files/test.pdf')
    @txt_file = Rack::Test::UploadedFile
                .new('spec/factories/files/test.txt')
    @png_file = Rack::Test::UploadedFile
                .new('spec/factories/images/rails-logo.png', 'image/jpeg')
  end

  it 'has default display_site_name as true' do
    siteSetting = SiteSetting.create
    expect(siteSetting.display_site_name).to eq(true)
  end

  it 'has default users_can_join as true' do
    siteSetting = SiteSetting.create
    expect(siteSetting.users_can_join).to eq(true)
  end

  it "has default site_name as 'My New Site'" do
    siteSetting = SiteSetting.create
    expect(siteSetting.site_name).to eq('My New Site')
  end

  it 'is valid with valid attributes' do
    siteSetting = SiteSetting.new(users_can_join: true,
                  site_name: 'my cool site! :) 2')
    expect(siteSetting).to be_valid
  end

  it 'is not valid with a site name > 50 char' do
    siteSetting = SiteSetting.new
    siteSetting.site_name = 'a' * 51
    expect(siteSetting).to_not be_valid
  end

  it 'is valid with jpeg default avatar' do
    siteSetting = SiteSetting.new
    siteSetting.default_avatar = @jpg_file
    expect(siteSetting).to be_valid
  end

  it 'is valid with png default avatar' do
    siteSetting = SiteSetting.new
    siteSetting.default_avatar = @png_file
    expect(siteSetting).to be_valid
  end

  it 'is not valid with pdf default avatar' do
    siteSetting = SiteSetting.new
    siteSetting.default_avatar = @pdf_file
    expect(siteSetting).to_not be_valid
  end

  it 'is not valid with text default avatar' do
    siteSetting = SiteSetting.new
    siteSetting.default_avatar = @txt_file
    expect(siteSetting).to_not be_valid
  end

  it 'is valid with jpeg logo' do
    siteSetting = SiteSetting.new
    siteSetting.logo = @jpg_file
    expect(siteSetting).to be_valid
  end

  it 'is valid with png logo' do
    siteSetting = SiteSetting.new
    siteSetting.logo = @png_file
    expect(siteSetting).to be_valid
  end

  it 'is not valid with pdf logo' do
    siteSetting = SiteSetting.new
    siteSetting.logo = @pdf_file
    expect(siteSetting).to_not be_valid
  end

  it 'is not valid with text logo' do
    siteSetting = SiteSetting.new
    siteSetting.logo = @txt_file
    expect(siteSetting).to_not be_valid
  end
end
