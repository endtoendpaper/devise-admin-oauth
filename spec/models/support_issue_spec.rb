require 'rails_helper'

RSpec.describe SupportIssue, type: :model do
  it 'is valid with valid data in plain text' do
    SiteSetting.first.update(rich_text_support_issues: 'false')
    issue = SupportIssue.new
    issue.user_id = FactoryBot.create(:user, :confirmed).id
    issue.summary = Faker::Lorem.sentence(word_count: 6)
    issue.body = Faker::Lorem.sentences(number: 20)
    issue.addressed = 0
    expect(issue).to be_valid
  end

  it 'is valid with valid data with rich text' do
    issue = SupportIssue.new
    issue.user_id = FactoryBot.create(:user, :confirmed).id
    issue.summary = Faker::Lorem.sentence(word_count: 6)
    issue.rich_text_body = Faker::Lorem.sentences(number: 20)
    issue.addressed = 0
    expect(issue).to be_valid
  end

  it 'is valid without a user' do
    SiteSetting.first.update(rich_text_support_issues: 'false')
    issue = SupportIssue.new
    issue.summary = Faker::Lorem.sentence(word_count: 6)
    issue.body = Faker::Lorem.sentences(number: 20)
    issue.addressed = 0
    expect(issue).to be_valid
  end

  it 'is invalid without a summary' do
    issue = SupportIssue.new
    issue.user_id = FactoryBot.create(:user, :confirmed).id
    issue.body = Faker::Lorem.sentences(number: 20)
    issue.addressed = 0
    expect(issue).to_not be_valid
  end

  it 'is invalid without a body and a rich_text_body' do
    issue = SupportIssue.new
    issue.user_id = FactoryBot.create(:user, :confirmed).id
    issue.summary = Faker::Lorem.sentence(word_count: 6)
    issue.addressed = 0
    expect(issue).to_not be_valid
  end

  it 'is invalid with both a body and a rich_text_body' do
    issue = SupportIssue.new
    issue.user_id = FactoryBot.create(:user, :confirmed).id
    issue.summary = Faker::Lorem.sentence(word_count: 6)
    issue.body = Faker::Lorem.sentences(number: 20)
    issue.rich_text_body = Faker::Lorem.sentences(number: 20)
    issue.addressed = 0
    expect(issue).to_not be_valid
  end

  it 'is invalid with a summary less than 3 char' do
    issue = SupportIssue.new
    issue.user_id = FactoryBot.create(:user, :confirmed).id
    issue.summary = 'aa'
    issue.body = Faker::Lorem.sentences(number: 20)
    issue.addressed = 0
    expect(issue).to_not be_valid
  end

  it 'is invalid with a body less than 10 char' do
    issue = SupportIssue.new
    issue.user_id = FactoryBot.create(:user, :confirmed).id
    issue.summary = Faker::Lorem.sentence(word_count: 6)
    issue.body = 'a' * 9
    issue.addressed = 0
    expect(issue).to_not be_valid
  end

  it 'is invalid with a summary more than 300 char' do
    issue = SupportIssue.new
    issue.user_id = FactoryBot.create(:user, :confirmed).id
    issue.summary = 'a' * 256
    issue.body = Faker::Lorem.sentences(number: 20)
    issue.addressed = 0
    expect(issue).to_not be_valid
  end

  it 'is invalid with a body more than 2000 char' do
    issue = SupportIssue.new
    issue.user_id = FactoryBot.create(:user, :confirmed).id
    issue.summary = Faker::Lorem.sentence(word_count: 6)
    issue.body = 'a' * 2001
    issue.addressed = 0
    expect(issue).to_not be_valid
  end

  it 'is invalid with a rich_text_body more than 2000 char' do
    issue = SupportIssue.new
    issue.user_id = FactoryBot.create(:user, :confirmed).id
    issue.summary = Faker::Lorem.sentence(word_count: 6)
    issue.rich_text_body = 'a' * 2001
    issue.addressed = 0
    expect(issue).to_not be_valid
  end

  it 'is invalid with rich_text_body when rich_text_support_issues = false' do
    SiteSetting.first.update(rich_text_support_issues: false)
    issue = SupportIssue.new
    issue.user_id = FactoryBot.create(:user, :confirmed).id
    issue.summary = Faker::Lorem.sentence(word_count: 6)
    issue.rich_text_body = Faker::Lorem.sentences(number: 20)
    issue.addressed = 0
    expect(issue).to_not be_valid
  end

  it 'does destroys replies if issue is destroyed' do
    user1 = FactoryBot.create(:user)
    user2 = FactoryBot.create(:user)
    support_issue = FactoryBot.create(:support_issue, :rich_text, user_id: user1.id)
    FactoryBot.create(:support_reply, :rich_text, user_id: user2.id, support_issue_id: support_issue.id)
    FactoryBot.create(:support_reply, :rich_text, user_id: user2.id, support_issue_id: support_issue.id)
    expect do
      support_issue.destroy
    end.to change(SupportReply, :count).by(-2)
  end

  it 'does destroys comments if issue is destroyed' do
    user1 = FactoryBot.create(:user)
    user2 = FactoryBot.create(:user)
    support_issue = FactoryBot.create(:support_issue, :rich_text, user_id: user1.id)
    FactoryBot.create(:support_comment, :rich_text, user_id: user2.id, support_issue_id: support_issue.id)
    FactoryBot.create(:support_comment, :rich_text, user_id: user2.id, support_issue_id: support_issue.id)
    expect do
      support_issue.destroy
    end.to change(SupportComment, :count).by(-2)
  end

  it 'does destroys notes if issue is destroyed' do
    user1 = FactoryBot.create(:user)
    user2 = FactoryBot.create(:user)
    support_issue = FactoryBot.create(:support_issue, :rich_text, user_id: user1.id)
    FactoryBot.create(:support_note, :rich_text, user_id: user2.id, support_issue_id: support_issue.id)
    FactoryBot.create(:support_note, :rich_text, user_id: user2.id, support_issue_id: support_issue.id)
    expect do
      support_issue.destroy
    end.to change(SupportNote, :count).by(-2)
  end
end
