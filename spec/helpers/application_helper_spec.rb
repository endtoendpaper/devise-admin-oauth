require "rails_helper"

RSpec.describe ApplicationHelper, type: :helper do
  include ApplicationHelper

  it "returns full title" do
    expect(SiteSetting.site_name).to eq(full_title(""))
    expect('Help | ' + SiteSetting.site_name).to eq(full_title("Help"))
  end
end
