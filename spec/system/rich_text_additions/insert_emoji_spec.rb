require 'rails_helper'

describe 'Insert emoji into note', type: :system do
  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @help_section = FactoryBot.create(:help_section)
  end

  scenario 'using emoji picker', js: true do
    sign_in @admin
    visit edit_help_section_path(@help_section)
    expect(page).to have_css('emoji-picker', visible: false)
    find(:css, 'button.trix-button--icon-smiley').click
    expect(page).to have_css('emoji-picker', visible: true)
    find(:css, 'button.trix-button--icon-smiley').click
    expect(page).to have_css('emoji-picker', visible: false)
  end
end
