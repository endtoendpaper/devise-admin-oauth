require 'rails_helper'

describe 'Add and remove tables', type: :system do
  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @help_section = FactoryBot.create(:help_section)
  end

  scenario 'to note', js: true do
    sign_in @admin
    visit edit_help_section_path(@help_section)
    find(:css, 'button.trix-button--icon-table').click
    expect(page).to have_button('Add Row')
    expect(page).to have_button('Add Column')
    expect(page).to have_button('Bold First Row')
    expect(page).to_not have_button('Remove Row')
    expect(page).to_not have_button('Remove Column')
    expect(page).to_not have_button('Unbold First Row')
    page.find_button('Add Row', visible: :all).click
    page.find_button('Add Row', visible: :all).click
    expect(page).to have_button('Remove Row')
    page.find_button('Add Column', visible: :all).click
    page.find_button('Add Column', visible: :all).click
    expect(page).to have_button('Remove Column')
    page.find_button('Bold First Row', visible: :all).click
    page.find_button('Unbold First Row', visible: :all).click
    click_button 'Submit'
    expect(page).to have_css('table')
  end
end
