require 'rails_helper'

describe 'User locks account', type: :system do
  let(:password) { 'badpass' }

  before(:each) do
    @user = FactoryBot.create(:user, :confirmed)
    visit new_user_session_path
  end

  scenario 'after 19 failed auth attempts' do
    18.times do
      fill_in 'user_login', with: @user.email
      fill_in 'user_password', with: password
      click_button 'Login'
      expect(page).to have_content('Invalid Login or password.')
      expect(current_path).to eq('/users/sign_in')
      expect(page).to_not have_selector('a', text: 'My Account')
      expect(Devise.mailer.deliveries.count).to eq 0
      visit new_user_session_path
      @user.reload
      expect(@user.locked_at).to be_nil
    end
    fill_in 'user_login', with: @user.email
    fill_in 'user_password', with: password
    click_button 'Login'
    expect(page).to have_content('You have one more attempt before your account is locked.')
    expect(current_path).to eq('/users/sign_in')
    expect(page).to_not have_selector('a', text: 'My Account')
    expect(Devise.mailer.deliveries.count).to eq 0
    visit new_user_session_path
    @user.reload
    fill_in 'user_login', with: @user.email
    fill_in 'user_password', with: password
    click_button 'Login'
    expect(page).to have_content('Your account is locked.')
    expect(current_path).to eq('/users/sign_in')
    expect(page).to_not have_selector('a', text: 'My Account')
    expect(Devise.mailer.deliveries.count).to eq 1
    visit new_user_session_path
    @user.reload
    expect(@user.locked_at).to_not be_nil
    expect(Devise.mailer.deliveries.last.to).to include(@user.email)
    expect(Devise.mailer.deliveries.last.subject).to eq('Unlock instructions')
  end

  scenario 'not after only 19 failed auth attempts' do
    18.times do
      fill_in 'user_login', with: @user.email
      fill_in 'user_password', with: password
      click_button 'Login'
      expect(page).to have_content('Invalid Login or password.')
      expect(current_path).to eq('/users/sign_in')
      expect(page).to_not have_selector('a', text: 'My Account')
      expect(Devise.mailer.deliveries.count).to eq 0
      visit new_user_session_path
      @user.reload
      expect(@user.locked_at).to be_nil
    end
    fill_in 'user_login', with: @user.email
    fill_in 'user_password', with: password
    click_button 'Login'
    expect(page).to have_content('You have one more attempt before your account is locked.')
    expect(current_path).to eq('/users/sign_in')
    expect(page).to_not have_selector('a', text: 'My Account')
    expect(Devise.mailer.deliveries.count).to eq 0
    visit new_user_session_path
    @user.reload
    expect(@user.locked_at).to be_nil
  end
end
