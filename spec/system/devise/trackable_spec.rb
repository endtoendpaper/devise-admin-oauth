require 'rails_helper'

describe 'Trackable', type: :system do
  scenario 'updates sign_in_count when users signs in again' do
    user = FactoryBot.create(:user, :confirmed)
    expect(user.sign_in_count).to eq(0)
    visit new_user_session_path
    fill_in 'user_login', with: user.email
    fill_in 'user_password', with: user.password
    click_button 'Login'
    click_link 'My Account'
    click_link 'Logout'
    user.reload
    expect(user.sign_in_count).to eq(1)
  end

  scenario 'updates current_sign_in_at when users signs in again' do
    user = FactoryBot.create(:user, :confirmed)
    pre_value = user.current_sign_in_at
    visit new_user_session_path
    fill_in 'user_login', with: user.email
    fill_in 'user_password', with: user.password
    click_button 'Login'
    visit root_path
    user.reload
    expect(user.current_sign_in_at).to_not eq(pre_value)
  end

  scenario 'updates last_sign_in_at when users signs in again' do
    user = FactoryBot.create(:user, :confirmed)
    pre_value = user.last_sign_in_at
    visit new_user_session_path
    fill_in 'user_login', with: user.email
    fill_in 'user_password', with: user.password
    click_button 'Login'
    click_link 'My Account'
    click_link 'Logout'
    visit root_path
    user.reload
    expect(user.last_sign_in_at).to_not eq(pre_value)
  end

  scenario 'updates current_sign_in_ip when users signs in again' do
    user = FactoryBot.create(:user, :confirmed)
    pre_value = user.current_sign_in_ip
    visit new_user_session_path
    fill_in 'user_login', with: user.email
    fill_in 'user_password', with: user.password
    click_button 'Login'
    visit root_path
    user.reload
    expect(user.current_sign_in_ip).to_not eq(pre_value)
  end

  scenario 'updates last_sign_in_ip when users signs in again' do
    user = FactoryBot.create(:user, :confirmed)
    pre_value = user.last_sign_in_ip
    visit new_user_session_path
    fill_in 'user_login', with: user.email
    fill_in 'user_password', with: user.password
    click_button 'Login'
    click_link 'My Account'
    click_link 'Logout'
    visit root_path
    user.reload
    expect(user.last_sign_in_ip).to_not eq(pre_value)
  end
end
