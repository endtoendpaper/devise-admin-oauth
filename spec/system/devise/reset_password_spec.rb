require 'rails_helper'

describe 'User resets password', type: :system do
  let(:password) { Faker::Internet.password(min_length: 6) }

  before(:each) do
    @user = FactoryBot.create(:user, :confirmed)
  end

  scenario 'with valid data' do
    reset_password_token = @user.send_reset_password_instructions
    expect(Devise.mailer.deliveries.count).to eq 1
    url = edit_user_password_path
    visit url + '?reset_password_token=' + reset_password_token
    expect(page).to have_selector('h1', text: 'Change Your Password')
    expect(page).to have_link('Login')
    expect(page).to have_link("Didn't receive confirmation instructions?")
    expect(page).to have_link("Didn't receive unlock instructions?")
    fill_in 'user_password', with: password
    fill_in 'user_password_confirmation', with: password
    click_button 'Change My Password'
    expect(page).to have_content('Your password has been changed successfully. You are now signed in.')
    expect(current_path).to eq('/')
    expect(page).to have_selector('a', text: 'My Account')
    Devise.mailer.deliveries.count == 2
  end

  scenario 'over six hours after reset email sent', js: true do
    reset_password_token = @user.send_reset_password_instructions
    @user.update(reset_password_sent_at: DateTime.now - (6.0 / 24))
    expect(Devise.mailer.deliveries.count).to eq 1
    url = edit_user_password_path
    visit url + '?reset_password_token=' + reset_password_token
    fill_in 'user_password', with: password
    fill_in 'user_password_confirmation', with: password
    click_button 'Change My Password'
    expect(page).to have_content('Reset password token has expired, please request a new one')
    expect(current_path).to eq('/users/password/edit')
    expect(page).to_not have_selector('a', text: 'My Account')
    expect(Devise.mailer.deliveries.count).to eq 1
  end

  scenario 'with invalid reset_password_token', js: true do
    reset_password_token = @user.send_reset_password_instructions
    expect(Devise.mailer.deliveries.count).to eq 1
    url = edit_user_password_path
    visit url + '?reset_password_token=' + '11111'
    expect(page).to have_selector('h1', text: 'Change Your Password')
    fill_in 'user_password', with: password
    fill_in 'user_password_confirmation', with: password
    click_button 'Change My Password'
    expect(page).to have_content('1 error prohibited this user from being saved:')
    expect(page).to have_content('Reset password token is invalid')
    expect(current_path).to eq('/users/password/edit')
    expect(page).to_not have_selector('a', text: 'My Account')
    expect(Devise.mailer.deliveries.count).to eq 1
  end

  scenario 'without reset_password_token' do
    visit edit_user_password_path
    expect(page).to have_content("You can't access this page without coming from a password reset email. If you do come from a password reset email, please make sure you used the full URL provided.")
    expect(current_path).to eq('/users/sign_in')
    expect(page).to_not have_selector('a', text: 'My Account')
    expect(Devise.mailer.deliveries.count).to eq 0
  end

  scenario 'with password mismatch', js: true do
    reset_password_token = @user.send_reset_password_instructions
    expect(Devise.mailer.deliveries.count).to eq 1
    url = edit_user_password_path
    visit url + '?reset_password_token=' + reset_password_token
    fill_in 'user_password', with: 'mypass'
    fill_in 'user_password_confirmation', with: 'notmypass'
    click_button 'Change My Password'
    expect(page).to have_content('1 error prohibited this user from being saved:')
    expect(page).to have_content("Password confirmation doesn't match Password")
    expect(current_path).to eq('/users/password/edit')
    expect(page).to_not have_selector('a', text: 'My Account')
    expect(Devise.mailer.deliveries.count).to eq 1
  end

  scenario 'user is already signed in' do
    sign_in @user
    visit edit_user_password_path
    expect(page).to have_content('You are already signed in.')
    expect(current_path).to eq('/')
    expect(page).to have_selector('a', text: 'My Account')
    expect(Devise.mailer.deliveries.count).to eq 0
  end
end
