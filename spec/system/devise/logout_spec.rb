require 'rails_helper'

describe 'User logs out', type: :system do
  scenario 'after logging in', js: true do
    user = FactoryBot.create(:user, :confirmed)
    sign_in user
    visit root_path
    click_link 'My Account'
    click_link 'Logout'
    expect(page).to_not have_selector('a', text: 'My Account')
  end
end
