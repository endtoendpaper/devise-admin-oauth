require 'rails_helper'

describe 'User forgot password', type: :system do
  let(:email) { Faker::Internet.email }
  let(:password) { Faker::Internet.password(min_length: 6) }

  before(:each) do
    visit new_user_password_path
  end

  scenario 'with valid data' do
    expect(page).to have_selector('h1', text: 'Forgot Your Password?')
    expect(page).to have_link('Login')
    expect(page).to have_link("Didn't receive confirmation instructions?")
    expect(page).to have_link("Didn't receive unlock instructions?")
    user = FactoryBot.create(:user, :confirmed)
    expect(Devise.mailer.deliveries.count).to eq 0
    fill_in 'user_email', with: user.email
    click_button 'Send Me Reset Password Instructions'
    expect(page).to have_content('You will receive an email with instructions on how to reset your password in a few minutes.')
    expect(current_path).to eq('/users/sign_in')
    expect(page).to_not have_selector('a', text: 'My Account')
    expect(Devise.mailer.deliveries.count).to eq 1
    expect(Devise.mailer.deliveries.last.to).to include(user.email)
    expect(Devise.mailer.deliveries.last.subject).to eq('Reset password instructions')
    expect(Devise.mailer.deliveries.last.body).to include(user.reset_password_token)
  end

  scenario 'with invalid email', js: true do
    fill_in 'user_email', with: 'me@hello'
    click_button 'Send Me Reset Password Instructions'
    expect(page).to have_content('1 error prohibited this user from being saved:')
    expect(page).to have_content('Email not found')
    expect(current_path).to eq('/users/password/new')
    expect(page).to_not have_selector('a', text: 'My Account')
    expect(Devise.mailer.deliveries.count).to eq 0
  end

  scenario 'with blank email', js: true do
    click_button 'Send Me Reset Password Instructions'
    expect(page).to have_content('1 error prohibited this user from being saved:')
    expect(page).to have_content("Email can't be blank")
    expect(current_path).to eq('/users/password/new')
    expect(page).to_not have_selector('a', text: 'My Account')
    expect(Devise.mailer.deliveries.count).to eq 0
  end

  scenario 'user is already signed in' do
    user = FactoryBot.create(:user, :confirmed)
    sign_in user
    visit new_user_password_path
    expect(page).to have_content('You are already signed in.')
    expect(current_path).to eq('/')
    expect(page).to have_selector('a', text: 'My Account')
    expect(Devise.mailer.deliveries.count).to eq 0
  end
end
