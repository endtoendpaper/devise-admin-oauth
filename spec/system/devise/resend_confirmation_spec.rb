require 'rails_helper'

describe 'User resends confirmation token', type: :system do
  let(:email) { Faker::Internet.email }

  scenario 'with valid data' do
    user = FactoryBot.create(:user)
    expect(Devise.mailer.deliveries.count).to eq 1
    visit new_user_confirmation_path
    expect(page).to have_selector('h1', text: 'Resend Confirmation Instructions')
    fill_in 'user_email', with: user.email
    click_button 'Resend Confirmation Instructions'
    expect(page).to have_content('You will receive an email with instructions for how to confirm your email address in a few minutes.')
    expect(current_path).to eq('/users/sign_in')
    expect(page).to_not have_selector('a', text: 'My Account')
    expect(Devise.mailer.deliveries.count).to eq 2
    expect(Devise.mailer.deliveries.last.to).to include(user.email)
    expect(Devise.mailer.deliveries.last.subject).to eq('Confirmation instructions')
  end

  scenario 'with invalid email', js: true do
    user = FactoryBot.create(:user)
    expect(Devise.mailer.deliveries.count).to eq 1
    visit new_user_confirmation_path
    fill_in 'user_email', with: email
    click_button 'Resend Confirmation Instructions'
    expect(page).to have_content('1 error prohibited this user from being saved:')
    expect(page).to have_content('Email not found')
    expect(current_path).to eq('/users/confirmation/new')
    expect(page).to_not have_selector('a', text: 'My Account')
    expect(Devise.mailer.deliveries.count).to eq 1
  end

  scenario 'already confirmed', js: true do
    user = FactoryBot.create(:user, :confirmed)
    visit new_user_confirmation_path
    fill_in 'user_email', with: user.email
    click_button 'Resend Confirmation Instructions'
    expect(page).to have_content('1 error prohibited this user from being saved:')
    expect(page).to have_content('Email was already confirmed, please try signing in')
    expect(current_path).to eq('/users/confirmation/new')
    expect(page).to_not have_selector('a', text: 'My Account')
    expect(Devise.mailer.deliveries.count).to eq 0
  end

  scenario 'already logged in' do
    user = FactoryBot.create(:user, :confirmed)
    sign_in user
    visit edit_user_password_path
    expect(page).to have_content('You are already signed in.')
    expect(current_path).to eq('/')
    expect(page).to have_selector('a', text: 'My Account')
    expect(Devise.mailer.deliveries.count).to eq 0
  end
end
