require 'rails_helper'

describe 'User signs up when registrations are allowed', type: :system do
  let(:email) { Faker::Internet.email }
  let(:password) { Faker::Internet.password(min_length: 6) }
  let(:username) { Faker::Internet.username(specifier: 1..50, separators: ['-','_','0','123','9']) }

  before(:each) do
    visit new_user_registration_path
  end

  scenario 'with valid data' do
    expect(page).to have_selector('h1', text: 'Sign Up')
    expect(page).to have_link('Login')
    expect(page).to have_link("Didn't receive confirmation instructions?")
    expect(page).to have_link("Didn't receive unlock instructions?")
    fill_in 'user_email', with: email
    fill_in 'user_username', with: username
    fill_in 'user_password', with: password
    fill_in 'user_password_confirmation', with: password
    click_button 'Sign up'
    expect(page).to have_content('A message with a confirmation link has been sent to your email address. Please follow the link to activate your account.')
    expect(current_path).to eq('/')
    expect(page).to_not have_selector('a', text: 'My Account')
    expect(Devise.mailer.deliveries.count).to eq 1
    expect(Devise.mailer.deliveries.last.to).to include(email)
    expect(Devise.mailer.deliveries.last.subject).to eq('Confirmation instructions')
  end

  scenario 'when email already exists', js: true do
    user = FactoryBot.create(:user, :confirmed)
    fill_in 'user_email', with: user.email
    fill_in 'user_username', with: username
    fill_in 'user_password', with: password
    fill_in 'user_password_confirmation', with: password
    click_button 'Sign up'
    expect(page).to have_content('1 error prohibited this user from being saved:')
    expect(page).to have_content('Email has already been taken')
    expect(current_path).to eq('/users/sign_up')
    expect(page).to_not have_selector('a', text: 'My Account')
    expect(Devise.mailer.deliveries.count).to eq 0
  end

  scenario 'when username already exists', js: true do
    user = FactoryBot.create(:user, :confirmed)
    fill_in 'user_email', with: email
    fill_in 'user_username', with: user.username
    fill_in 'user_password', with: password
    fill_in 'user_password_confirmation', with: password
    click_button 'Sign up'
    expect(page).to have_content('1 error prohibited this user from being saved:')
    expect(page).to have_content('Username has already been taken')
    expect(current_path).to eq('/users/sign_up')
    expect(page).to_not have_selector('a', text: 'My Account')
    expect(Devise.mailer.deliveries.count).to eq 0
  end

  scenario 'when password is too short', js: true do
    fill_in 'user_email', with: email
    fill_in 'user_username', with: username
    fill_in 'user_password', with: '12345'
    fill_in 'user_password_confirmation', with: '12345'
    click_button 'Sign up'
    expect(page).to have_content('1 error prohibited this user from being saved:')
    expect(page).to have_content('Password is too short (minimum is 6 characters)')
    expect(current_path).to eq('/users/sign_up')
    expect(page).to_not have_selector('a', text: 'My Account')
    expect(Devise.mailer.deliveries.count).to eq 0
  end

  scenario "when passwords don't match", js: true do
    fill_in 'user_email', with: email
    fill_in 'user_username', with: username
    fill_in 'user_password', with: '123456'
    fill_in 'user_password_confirmation', with: '123457'
    click_button 'Sign up'
    expect(page).to have_content('1 error prohibited this user from being saved:')
    expect(page).to have_content("Password confirmation doesn't match Password")
    expect(current_path).to eq('/users/sign_up')
    expect(page).to_not have_selector('a', text: 'My Account')
    expect(Devise.mailer.deliveries.count).to eq 0
  end

  scenario 'user is already signed in' do
    user = FactoryBot.create(:user, :confirmed)
    sign_in user
    visit new_user_registration_path
    expect(page).to have_content('You are already signed in.')
    expect(current_path).to eq('/')
    expect(page).to have_selector('a', text: 'My Account')
    expect(Devise.mailer.deliveries.count).to eq 0
  end

  scenario 'when users are not allowed to sign up' do
    SiteSetting.first.update(users_can_join: false)
    visit new_user_session_path
    expect(page).to_not have_link('Sign up')
  end
end
