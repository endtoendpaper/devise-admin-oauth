require 'rails_helper'

describe 'User resends unlock instructions', type: :system do
  scenario 'with valid data' do
    user = FactoryBot.create(:user, :confirmed, :locked)
    visit new_user_unlock_path
    expect(page).to have_selector('h1', text: 'Resend Unlock Instructions')
    expect(page).to have_link('Login')
    expect(page).to have_link("Didn't receive confirmation instructions?")
    expect(page).to have_link('Forgot your password?')
    expect(Devise.mailer.deliveries.count).to eq 0
    fill_in 'user_email', with: user.email
    click_button 'Resend Unlock Instructions'
    expect(page).to have_content('You will receive an email with instructions for how to unlock your account in a few minutes.')
    expect(current_path).to eq('/users/sign_in')
    expect(page).to_not have_selector('a', text: 'My Account')
    expect(Devise.mailer.deliveries.count).to eq 1
    expect(Devise.mailer.deliveries.last.to).to include(user.email)
    expect(Devise.mailer.deliveries.last.subject).to eq('Unlock instructions')
  end

  scenario 'with unregistered email', js: true do
    expect(Devise.mailer.deliveries.count).to eq 0
    visit new_user_unlock_path
    fill_in 'user_email', with: 'bad@email'
    click_button 'Resend Unlock Instructions'
    expect(page).to have_content('1 error prohibited this user from being saved:')
    expect(page).to have_content('Email not found')
    expect(current_path).to eq('/users/unlock/new')
    expect(page).to_not have_selector('a', text: 'My Account')
    expect(Devise.mailer.deliveries.count).to eq 0
  end

  scenario 'with unconfirmed email', js: true do
    user = FactoryBot.create(:user)
    expect(Devise.mailer.deliveries.count).to eq 1
    visit new_user_unlock_path
    fill_in 'user_email', with: user.email
    click_button 'Resend Unlock Instructions'
    expect(page).to have_content('1 error prohibited this user from being saved:')
    expect(page).to have_content('Email was not locked')
    expect(current_path).to eq('/users/unlock/new')
    expect(page).to_not have_selector('a', text: 'My Account')
    expect(Devise.mailer.deliveries.count).to eq 1
  end

  scenario 'with unlocked account', js: true do
    user = FactoryBot.create(:user, :confirmed)
    expect(Devise.mailer.deliveries.count).to eq 0
    visit new_user_unlock_path
    fill_in 'user_email', with: user.email
    click_button 'Resend Unlock Instructions'
    expect(page).to have_content('1 error prohibited this user from being saved:')
    expect(page).to have_content('Email was not locked')
    expect(current_path).to eq('/users/unlock/new')
    expect(page).to_not have_selector('a', text: 'My Account')
    expect(Devise.mailer.deliveries.count).to eq 0
  end

  scenario 'already signed in' do
    user = FactoryBot.create(:user, :confirmed)
    expect(Devise.mailer.deliveries.count).to eq 0
    sign_in user
    visit new_user_unlock_path
    expect(page).to have_content('You are already signed in.')
    expect(current_path).to eq('/')
    expect(page).to have_selector('a', text: 'My Account')
    expect(Devise.mailer.deliveries.count).to eq 0
  end
end
