require 'rails_helper'

describe 'Submit issue', type: :system do
  let(:summary) { Faker::Lorem.sentence(word_count: 3) }
  let(:body) { Faker::Lorem.sentence(word_count: 25) }

  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @admin2 = FactoryBot.create(:user, :confirmed, :admin)
    @user = FactoryBot.create(:user, :confirmed)
  end

  scenario 'as user with rich text', js: true do
    SiteSetting.first.update(rich_text_support_issues: 'true')
    expect(ActionMailer::Base.deliveries.count).to eq 0
    sign_in @user
    visit help_path
    click_link 'Get Support'
    fill_in 'support_issue_summary', with: summary
    fill_in_trix_editor 'support_issue_rich_text_body_trix_input_support_issue', with: body
    click_button 'Submit'
    expect(page).to have_content('Support form successfully submitted.')
    expect(ActionMailer::Base.deliveries.count).to eq 2
    expect(current_path).to include('/help')
  end

  scenario 'as user with plain text' do
    SiteSetting.first.update(rich_text_support_issues: 'false')
    expect(ActionMailer::Base.deliveries.count).to eq 0
    sign_in @user
    visit help_path
    click_link 'Get Support'
    fill_in 'support_issue_summary', with: summary
    fill_in 'support_issue_body', with: body
    click_button 'Submit'
    expect(page).to have_content('Support form successfully submitted.')
    expect(ActionMailer::Base.deliveries.count).to eq 2
    expect(current_path).to include('/help')
  end

  scenario 'with invalid data', js: true do
    SiteSetting.first.update(rich_text_support_issues: 'true')
    expect(ActionMailer::Base.deliveries.count).to eq 0
    sign_in @user
    visit help_path
    click_link 'Get Support'
    fill_in 'support_issue_summary', with: summary
    click_button 'Submit'
    expect(page).to have_content('2 errors prohibited this support issue from being saved:')
    expect(page).to have_content("Details can't blank")
    expect(page).to have_content('Rich text body is too short (minimum is 10 characters)')
    expect(ActionMailer::Base.deliveries.count).to eq 0
    expect(current_path).to include('/support/new')
  end
end
