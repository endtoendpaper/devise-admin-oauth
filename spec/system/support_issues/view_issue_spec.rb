require 'rails_helper'

describe 'View issue', type: :system do

  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @user1 = FactoryBot.create(:user, :confirmed)
    @support_issue1 = FactoryBot.create(:support_issue, :rich_text, rich_text_body: "support issue 1", user_id: @user1.id)
    @note = FactoryBot.create(:support_note, :rich_text, rich_text_body: "my support note 1", user_id: @admin.id, support_issue_id: @support_issue1.id)
    @reply = FactoryBot.create(:support_reply, :rich_text, rich_text_body: "my support reply 1", user_id: @admin.id, support_issue_id: @support_issue1.id)
    @comment = FactoryBot.create(:support_comment, :rich_text, rich_text_body: "my support comment 1", user_id: @user1.id, support_issue_id: @support_issue1.id)
    @support_issue2 = FactoryBot.create(:support_issue, :addressed, :rich_text, rich_text_body: "support issue 1", user_id: @user1.id)
    @note2 = FactoryBot.create(:support_note, :rich_text, rich_text_body: "my support note 1", user_id: @admin.id, support_issue_id: @support_issue2.id)
    @reply2 = FactoryBot.create(:support_reply, :rich_text, rich_text_body: "my support reply 1", user_id: @admin.id, support_issue_id: @support_issue2.id)
    @comment2 = FactoryBot.create(:support_comment, :rich_text, rich_text_body: "my support comment 1", user_id: @user1.id, support_issue_id: @support_issue2.id)
  end

  scenario 'as user for open issue' do
    sign_in @user1
    visit support_issue_path(@support_issue1.id)
    expect(page).to_not have_button('Resolve')
    expect(page).to_not have_button('Reopen')
    expect(page).to have_content('From: @' + @user1.username + ' (' + @user1.email + ')')
    expect(page).to have_content('Summary: ' + @support_issue1.summary)
    expect(page).to have_content('Status: OPEN')
    expect(page).to have_content('support issue 1')
    expect(page).to_not have_content('From: @' + @admin.username + ' (' + @admin.email + ')')
    expect(page).to have_content('support reply 1')
    expect(page).to have_content('support comment 1')
    expect(page).to_not have_content('support note 1')
    expect(page).to have_content('Reply to Admins')
    expect(page).to_not have_content('Reply to User')
    expect(page).to_not have_content('Make Internal Note')
    expect(page).to_not have_content('View Status History')
  end

  scenario 'as user for resolved issue' do
    sign_in @user1
    visit support_issue_path(@support_issue2.id)
    expect(page).to_not have_button('Resolve')
    expect(page).to_not have_button('Reopen')
    expect(page).to have_content('From: @' + @user1.username + ' (' + @user1.email + ')')
    expect(page).to have_content('Summary: ' + @support_issue2.summary)
    expect(page).to have_content('Status: RESOLVED')
    expect(page).to have_content('support issue 1')
    expect(page).to_not have_content('From: @' + @admin.username + ' (' + @admin.email + ')')
    expect(page).to have_content('support reply 1')
    expect(page).to have_content('support comment 1')
    expect(page).to_not have_content('support note 1')
    expect(page).to have_content('Reply to Admins')
    expect(page).to_not have_content('Reply to User')
    expect(page).to_not have_content('Make Internal Note')
    expect(page).to_not have_content('View Status History')
  end

  scenario 'as admin for open issue' do
    sign_in @admin
    visit support_issue_path(@support_issue1.id)
    expect(page).to have_button('Resolve')
    expect(page).to_not have_button('Reopen')
    expect(page).to have_content('From: @' + @user1.username + ' (' + @user1.email + ')')
    expect(page).to have_content('Summary: ' + @support_issue1.summary)
    expect(page).to have_content('Status: OPEN')
    expect(page).to have_content('support issue 1')
    expect(page).to have_content('From: @' + @admin.username + ' (' + @admin.email + ')')
    expect(page).to have_content('support reply 1')
    expect(page).to have_content('support comment 1')
    expect(page).to have_content('support note 1')
    expect(page).to have_content('Reply to User')
    expect(page).to have_content('Make Internal Note')
    expect(page).to have_content('View Status History')
    expect(page).to_not have_content('Reply to Admins')
  end

  scenario 'as admin for resolved issue' do
    sign_in @admin
    visit support_issue_path(@support_issue2.id)
    expect(page).to_not have_button('Resolve')
    expect(page).to have_button('Reopen')
    expect(page).to have_content('From: @' + @user1.username + ' (' + @user1.email + ')')
    expect(page).to have_content('Summary: ' + @support_issue2.summary)
    expect(page).to have_content('Status: RESOLVED')
    expect(page).to have_content('support issue 1')
    expect(page).to have_content('From: @' + @admin.username + ' (' + @admin.email + ')')
    expect(page).to have_content('support reply 1')
    expect(page).to have_content('support comment 1')
    expect(page).to have_content('support note 1')
    expect(page).to_not have_content('Reply to User')
    expect(page).to have_content('Make Internal Note')
    expect(page).to have_content('View Status History')
    expect(page).to_not have_content('Reply to Admins')
  end
end
