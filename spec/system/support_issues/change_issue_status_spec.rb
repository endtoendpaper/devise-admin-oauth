require 'rails_helper'

describe 'Admin changes issue status', type: :system do
  before(:each) do
    SiteSetting.first.update(email_contact_form_to_admins: 'true')
    SiteSetting.first.update(email_issue_status_to_users: 'true')
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @admin2 = FactoryBot.create(:user, :confirmed, :admin)
    @user = FactoryBot.create(:user, :confirmed)
    @support_issue = FactoryBot.create(:support_issue, :rich_text, user_id: @user.id)
    @support_issue2 = FactoryBot.create(:support_issue, :rich_text, :addressed, user_id: @user.id)
  end

  scenario 'to resolve issue when email_issue_status_to_users is true', js: true do
    sign_in @admin
    expect(ActionMailer::Base.deliveries.count).to eq 0
    visit root_path
    find(:css, 'i.bi-gear-fill').click
    click_link 'Support Issues'
    click_link @support_issue.summary
    expect(page).to have_content('OPEN')
    expect(@support_issue.reload.addressed).to be false
    click_button 'Resolve'
    page.accept_alert
    expect(page).to have_content('Support issue status has been updated.')
    expect(page).to have_content('RESOLVED')
    expect(ActionMailer::Base.deliveries.count).to eq 2
    expect(@support_issue.reload.addressed).to be true
  end

  scenario 'to reopen issue when email_issue_status_to_users is true', js: true do
    sign_in @admin
    expect(ActionMailer::Base.deliveries.count).to eq 0
    visit root_path
    find(:css, 'i.bi-gear-fill').click
    click_link 'Support Issues'
    click_link @support_issue2.summary
    expect(page).to have_content('RESOLVED')
    expect(@support_issue2.reload.addressed).to be true
    click_button 'Reopen'
    page.accept_alert
    expect(page).to have_content('Support issue status has been updated.')
    expect(page).to have_content('OPEN')
    expect(ActionMailer::Base.deliveries.count).to eq 2
    expect(@support_issue2.reload.addressed).to be false
  end

  scenario 'to resolve issue when email_issue_status_to_users and email_contact_form_to_admins are false', js: true do
    sign_in @admin
    expect(ActionMailer::Base.deliveries.count).to eq 0
    visit root_path
    find(:css, 'i.bi-gear-fill').click
    click_link 'Site Settings'
    page.uncheck('site_setting_email_contact_form_to_admins')
    page.uncheck('site_setting_email_issue_status_to_users')
    click_button 'Update'
    visit current_path
    find(:css, 'i.bi-gear-fill').click
    click_link 'Support Issues'
    click_link @support_issue.summary
    expect(page).to have_content('OPEN')
    expect(@support_issue.reload.addressed).to be false
    click_button 'Resolve'
    page.accept_alert
    expect(page).to have_content('Support issue status has been updated.')
    expect(page).to have_content('RESOLVED')
    expect(ActionMailer::Base.deliveries.count).to eq 0
    expect(@support_issue.reload.addressed).to be true
  end

  scenario 'to reopen issue when email_issue_status_to_users and email_contact_form_to_admins are false', js: true do
    sign_in @admin
    expect(ActionMailer::Base.deliveries.count).to eq 0
    visit root_path
    find(:css, 'i.bi-gear-fill').click
    click_link 'Site Settings'
    page.uncheck('site_setting_email_contact_form_to_admins')
    page.uncheck('site_setting_email_issue_status_to_users')
    click_button 'Update'
    visit current_path
    find(:css, 'i.bi-gear-fill').click
    click_link 'Support Issues'
    click_link @support_issue2.summary
    expect(page).to have_content('RESOLVED')
    expect(@support_issue2.reload.addressed).to be true
    click_button 'Reopen'
    page.accept_alert
    expect(page).to have_content('Support issue status has been updated.')
    expect(page).to have_content('OPEN')
    expect(ActionMailer::Base.deliveries.count).to eq 0
    expect(@support_issue2.reload.addressed).to be false
  end
end
