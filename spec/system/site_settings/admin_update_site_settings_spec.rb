require 'rails_helper'

describe 'admin updates site settings', type: :system do
  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
  end

  scenario 'with valid site name' do
    sign_in @admin
    visit root_path
    click_link 'Admin Tools'
    click_link 'Site Settings'
    fill_in 'site_setting_site_name', with: 'Kool n3w site name!'
    click_button 'Update'
    expect(page).to have_content('Site settings were successfully updated.')
    expect(html).to include('<a class="navbar-brand" href="/">Kool n3w site name!</a>')
  end

  scenario 'with valid site name not displayed' do
    sign_in @admin
    visit edit_site_setting_path(1)
    fill_in 'site_setting_site_name', with: 'Kool n3w site name!'
    page.uncheck('site_setting_display_site_name')
    click_button 'Update'
    expect(page).to have_content('Site settings were successfully updated.')
    visit root_path
    expect(html).to_not include('<a class="navbar-brand" href="/">Kool n3w site name!</a>')
  end

  scenario 'with invalid site name' do
    sign_in @admin
    visit edit_site_setting_path(1)
    new_site_site_name = 'a' * 51
    fill_in 'site_setting_site_name', with: new_site_site_name
    click_button 'Update'
    expect(page).to have_content('1 error prohibited settings from being saved:')
    expect(page).to have_content('Site name is too long (maximum is 50 characters)')
    expect(html).to_not include('<a class="navbar-brand" href="/">aaaaaaaaaaa')
  end

  scenario 'with clearing site name' do
    sign_in @admin
    visit edit_site_setting_path(1)
    fill_in 'site_setting_site_name', with: ''
    click_button 'Update'
    expect(page).to have_content('Site settings were successfully updated.')
    expect(html).to include('<a class="navbar-brand" href="/"></a>')
  end

  scenario "and doesn't allow users to join" do
    sign_in @admin
    visit edit_site_setting_path(1)
    expect(page).to have_checked_field('site_setting_users_can_join')
    page.uncheck('site_setting_users_can_join')
    click_button 'Update'
    expect(page).to have_content('Site settings were successfully updated.')
    sign_out @admin
    visit new_user_session_path
    expect(page).to_not have_link('Sign up')
    visit new_user_registration_path
    expect(current_path).to include('/')
  end
end
