require 'rails_helper'

describe 'Change relationships', type: :system do
  before(:each) do
    @user1 = FactoryBot.create(:user, :confirmed)
    @user2 = FactoryBot.create(:user, :confirmed)
  end

  scenario 'following and unfollowing users' do
    sign_in @user1
    visit user_path(@user2)
    expect(page).to have_link('0 following', minimum: 1)
    expect(page).to have_link('0 followers', minimum: 1)
    click_button("Follow")
    expect(page).to have_link('0 following', minimum: 1)
    expect(page).to have_link('1 followers', minimum: 1)
    visit user_path(@user1)
    expect(page).to have_link('1 following', minimum: 1)
    expect(page).to have_link('0 followers', minimum: 1)
    visit user_path(@user2)
    click_button("Unfollow")
    expect(page).to have_link('0 following', minimum: 1)
    expect(page).to have_link('0 followers', minimum: 1)
    visit user_path(@user1)
    expect(page).to have_link('0 following', minimum: 1)
    expect(page).to have_link('0 followers', minimum: 1)
  end
end
