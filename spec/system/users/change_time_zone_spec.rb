require 'rails_helper'

describe 'Changes timezone', type: :system do
  before(:each) do
    @user = FactoryBot.create(:user, :confirmed, time_zone: 'UTC')
    @support_issue = FactoryBot.create(:support_issue, :rich_text, user_id: @user.id)
  end

  scenario 'as user', js: true do
    sign_in @user
    visit support_issue_path(@support_issue)
    expect(page).to have_content('UTC')
    visit edit_user_registration_path
    find_by_id("user_time_zone").find("option[value='Hawaii']").click
    click_button 'Update'
    expect(page).to have_content('Your account has been updated successfully.')
    visit support_issue_path(@support_issue)
    expect(page).to_not have_content('UTC')
    expect(page).to have_content('HST')
  end

  scenario 'as admin for a user', js: true do
    admin = FactoryBot.create(:user, :confirmed, :admin)
    sign_in admin
    visit user_path(@user)
    expect(page).to have_content('Timezone: UTC')
    click_link 'Edit user'
    find_by_id("user_time_zone").find("option[value='Hawaii']").click
    click_button 'Update'
    expect(page).to have_content('User was successfully updated.')
    expect(page).to have_content('Timezone: Hawaii')
  end
end
