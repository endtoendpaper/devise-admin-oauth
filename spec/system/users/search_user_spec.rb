require 'rails_helper'

describe 'Admin searches users', type: :system do
  before(:each) do
    reset_elasticsearch_indices

    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @user1 = FactoryBot.create(:user, :confirmed)
    @user2 = FactoryBot.create(:user, :confirmed)

    reindex_elasticsearch_data
  end

  scenario 'with email that exists', js: true do
    sign_in @user1
    visit users_path
    fill_in('search', with: @user2.email)
    click_button 'Search'
    expect(page).to_not have_content(@user2.email)
    expect(page).to_not have_content(@user2.username)
    expect(page).to_not have_content('Edit user')
  end

  scenario 'with username that exists', js: true do
    sign_in @user1
    visit users_path
    fill_in('search', with: @user2.username)
    click_button 'Search'
    expect(page).to_not have_content(@user2.email)
    expect(page).to have_content(@user2.username)
    expect(page).to_not have_content('Edit user')
  end

  scenario 'with username that does not exist and then clears search', js: true do
    sign_in @user1
    visit users_path
    expect(page).to_not have_content(@user2.email)
    expect(page).to have_content(@user2.username)
    expect(page).to_not have_content('Edit user')
    fill_in('search', with: 'dont@exist')
    click_button 'Search'
    expect(page).to_not have_content(@user2.email)
    expect(page).to_not have_content(@user2.username)
    expect(page).to_not have_content('Edit user')
    click_link 'Clear'
    expect(page).to_not have_content(@user2.email)
    expect(page).to have_content(@user2.username)
    expect(page).to_not have_content('Edit user')
  end
end
