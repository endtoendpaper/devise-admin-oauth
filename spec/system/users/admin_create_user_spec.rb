require 'rails_helper'

describe 'Admin creates user', type: :system do
  let(:email) { Faker::Internet.email }
  let(:password) { Faker::Internet.password(min_length: 6) }
  let(:username) { Faker::Internet.username(specifier: 1..50, separators: ['-','_','0','123','9']) }

  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
  end

  scenario 'with valid data' do
    sign_in @admin
    visit root_path
    click_link 'Admin Tools'
    click_link 'User Management'
    click_link 'New User'
    fill_in 'user_email', with: email
    fill_in 'user_username', with: username
    fill_in 'user_password', with: password
    fill_in 'user_password_confirmation', with: password
    click_button 'Create User'
    expect(page).to have_content('User created and confirmation email sent to their email address.')
    expect(current_path).to include('/users/')
    expect(Devise.mailer.deliveries.count).to eq 1
    expect(Devise.mailer.deliveries.last.to).to include(email)
    expect(Devise.mailer.deliveries.last.subject).to eq('Confirmation instructions')
    expect(User.ordered_by_id.last.admin).to be false
  end

  scenario 'with valid data and admin selected' do
    sign_in @admin
    visit new_user_path
    fill_in 'user_email', with: email
    page.check('Admin')
    fill_in 'user_username', with: username
    fill_in 'user_password', with: password
    fill_in 'user_password_confirmation', with: password
    click_button 'Create User'
    expect(page).to have_content('User created and confirmation email sent to their email address.')
    expect(current_path).to include('/users/')
    expect(Devise.mailer.deliveries.count).to eq 1
    expect(Devise.mailer.deliveries.last.to).to include(email)
    expect(Devise.mailer.deliveries.last.subject).to eq('Confirmation instructions')
    expect(User.ordered_by_id.last.admin).to be true
  end

  scenario 'with invalid email' do
    user = FactoryBot.create(:user, :confirmed)
    sign_in @admin
    visit new_user_path
    fill_in 'user_email', with: user.email
    fill_in 'user_username', with: username
    fill_in 'user_password', with: password
    fill_in 'user_password_confirmation', with: password
    click_button 'Create User'
    expect(page).to have_content('Email has already been taken')
    expect(current_path).to include('/users/new')
    expect(Devise.mailer.deliveries.count).to eq 0
  end

  scenario 'with invalid username' do
    sign_in @admin
    visit new_user_path
    fill_in 'user_email', with: email
    fill_in 'user_username', with: 'bad username'
    fill_in 'user_password', with: password
    fill_in 'user_password_confirmation', with: password
    click_button 'Create User'
    expect(page).to have_content('Username can only contain letters, numbers, dashes, or underscores')
    expect(current_path).to include('/users/new')
    expect(Devise.mailer.deliveries.count).to eq 0
  end

  scenario 'with password too short' do
    sign_in @admin
    visit new_user_path
    fill_in 'user_email', with: email
    fill_in 'user_username', with: username
    fill_in 'user_password', with: 'short'
    fill_in 'user_password_confirmation', with: 'short'
    click_button 'Create User'
    expect(page).to have_content('Password is too short (minimum is 6 characters)')
    expect(current_path).to include('/users/new')
    expect(Devise.mailer.deliveries.count).to eq 0
  end

  scenario 'with mismatched passwords' do
    sign_in @admin
    visit new_user_path
    fill_in 'user_email', with: email
    fill_in 'user_username', with: username
    fill_in 'user_password', with: 'mypass1'
    fill_in 'user_password_confirmation', with: 'mypass2'
    click_button 'Create User'
    expect(page).to have_content("Password confirmation doesn't match Password")
    expect(current_path).to include('/users/new')
    expect(Devise.mailer.deliveries.count).to eq 0
  end

  scenario 'without any data' do
    sign_in @admin
    visit new_user_path
    click_button 'Create User'
    expect(page).to have_content("Email can't be blank, Password can't be blank, Username can't be blank")
    expect(current_path).to include('/users/new')
    expect(Devise.mailer.deliveries.count).to eq 0
  end
end
