require 'rails_helper'

describe 'View user profile', type: :system do
  before(:each) do
    @user1 = FactoryBot.create(:user, :confirmed, :avatar)
    @user2 = FactoryBot.create(:user, :confirmed)
  end

  scenario 'when there is no site default avatar' do
    sign_in @user2
    visit user_path(@user1)
    expect(html).to include('<img src=')
    expect(html).to include('crying-cat.jpeg')
    expect(html).to_not include('default-avatar.jpg')
  end

  scenario 'when there is a site default avatar' do
    SiteSetting.first.update(default_avatar:
      Rack::Test::UploadedFile.new('spec/factories/images/default-avatar.jpg'))
    sign_in @user1
    visit user_path(@user1)
    expect(html).to include('<img src=')
    expect(html).to include('crying-cat.jpeg')
    expect(html).to_not include('default-avatar.jpg')
  end
end

