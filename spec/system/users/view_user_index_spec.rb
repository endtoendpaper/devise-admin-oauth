require 'rails_helper'

describe 'Views user index', type: :system do
  before(:each) do
    @user1 = FactoryBot.create(:user, :confirmed, :avatar)
    @user2 = FactoryBot.create(:user, :confirmed)
    @user3 = FactoryBot.create(:user, :confirmed)
    @user4 = FactoryBot.create(:user, :confirmed, :hide_relationships)
    @admin = FactoryBot.create(:user, :confirmed, :admin)
  end

  scenario 'as admin' do
    SiteSetting.first.update(follow_users: true)
    sign_in @admin
    visit users_path
    expect(html).to include('crying-cat.jpeg')
    expect(html).to include('<img src=')
    expect(page).to have_selector('h5', text: @admin.handle)
    expect(page).to have_selector('h5', text: @user1.handle)
    expect(page).to have_selector('h5', text: @user2.handle)
    expect(page).to have_selector('h5', text: @user3.handle)
    expect(page).to have_selector('h5', text: @user4.handle)
    expect(page).to have_link('0 following', minimum: 5)
    expect(page).to have_link('0 followers', minimum: 5)
    expect(page).to have_button('Follow', minimum: 4)
    expect(page).to have_link('Edit your profile', minimum: 1)
    expect(page).to_not have_content('Email: ')
    expect(page).to_not have_content('Email: ' + @user1.email)
    SiteSetting.first.update(follow_users: false)
    visit users_path
    expect(page).to_not have_link('0 following')
    expect(page).to_not have_link('0 followers')
    expect(page).to_not have_button('Follow')
    visit users_path
  end

  scenario 'as user' do
    SiteSetting.first.update(follow_users: true)
    sign_in @user1
    visit users_path
    expect(html).to include('crying-cat.jpeg')
    expect(html).to include('<img src=')
    expect(page).to have_selector('h5', text: @admin.handle)
    expect(page).to have_selector('h5', text: @user1.handle)
    expect(page).to have_selector('h5', text: @user2.handle)
    expect(page).to have_selector('h5', text: @user3.handle)
    expect(page).to have_selector('h5', text: @user4.handle)
    expect(page).to have_link('0 following', minimum: 4)
    expect(page).to have_link('0 followers', minimum: 4)
    expect(page).to have_button('Follow', minimum: 4)
    expect(page).to have_link('Edit your profile', minimum: 1)
    expect(page).to_not have_content('Email: ')
    expect(page).to_not have_content('Email: ' + @user1.email)
    SiteSetting.first.update(follow_users: false)
    visit users_path
    expect(page).to_not have_link('0 following')
    expect(page).to_not have_link('0 followers')
    expect(page).to_not have_button('Follow')
    visit users_path
  end

  scenario 'sees pagination with >5 users' do
    3.times do
      FactoryBot.create(:user, :confirmed)
    end
    visit users_path
    expect(page.body).to include('pagination pagination-sm justify-content-center')
  end
end
