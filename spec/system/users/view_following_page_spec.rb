require 'rails_helper'

describe 'Views user following page', type: :system do
  before(:each) do
    @user1 = FactoryBot.create(:user, :confirmed, :avatar)
    @user2 = FactoryBot.create(:user, :confirmed)
    @user3 = FactoryBot.create(:user, :confirmed)
    @user4 = FactoryBot.create(:user, :confirmed, :hide_relationships)
    @user5 = FactoryBot.create(:user, :confirmed, :hide_relationships)
    @user6 = FactoryBot.create(:user, :confirmed, :hide_relationships)
    @admin = FactoryBot.create(:user, :confirmed, :admin)

    @user1.follow(@user2)
    @user1.follow(@user3)
    @user1.follow(@user4)
    @user1.follow(@user5)
    @user1.follow(@user6)
  end

  scenario 'as admin' do
    sign_in @admin
    visit following_user_path(@user1)
    expect(page).to have_link('0 following', minimum: 5)
    expect(page).to have_link('5 following', minimum: 1)
    expect(page).to have_link('1 followers', minimum: 5)
    expect(page).to have_link('0 followers', minimum: 1)
    expect(page).to_not have_button('Unfollow')
    expect(page).to have_button('Follow', minimum: 6)
    expect(page).to_not have_content('Email:')
    expect(page).to_not have_content(@user1.email)
    expect(page).to_not have_content(@user2.email)
  end

  scenario 'as user' do
    sign_in @user4
    visit following_user_path(@user1)
    expect(page).to have_link('0 following', minimum: 3)
    expect(page).to have_link('5 following', minimum: 1)
    expect(page).to have_link('1 followers', minimum: 3)
    expect(page).to have_link('0 followers', minimum: 1)
    expect(page).to_not have_button('Unfollow')
    expect(page).to have_button('Follow', minimum: 5)
    expect(page).to_not have_content('Email:')
    expect(page).to_not have_content(@user1.email)
    expect(page).to_not have_content(@user2.email)
  end

  scenario 'sees pagination with >5 users' do
    @user1.follow(FactoryBot.create(:user, :confirmed, :admin))
    sign_in @admin
    visit following_user_path(@user1)
    expect(page.body).to include('pagination pagination-sm justify-content-center')
  end
end
