require 'rails_helper'

describe 'Admin edits help section', type: :system do
  let(:title) { Faker::Lorem.sentence(word_count: 3) }
  let(:content) { 'My edited help content!' }

  before(:each) do
    @admin = FactoryBot.create(:user, :confirmed, :admin)
    @help_section1 = FactoryBot.create(:help_section, content: "help_section1")
  end

  scenario 'with valid data', js: true do
    sign_in @admin
    visit help_path
    expect(page).to have_selector('h2', text: @help_section1.title)
    expect(page).to have_selector('li', text: @help_section1.title)
    click_link 'Edit'
    fill_in 'help_section_title', with: title
    fill_in_trix_editor 'help_section_content_trix_input_help_section_' + @help_section1.id.to_s, with: content
    click_button 'Submit'
    expect(page).to have_selector('h2', text: title)
    expect(page).to have_selector('li', text: title)
    expect(page).to have_content(content)
    expect(page).to have_content('Help section was successfully updated.')
  end

  scenario 'with invalid data' do
    sign_in @admin
    visit help_path
    expect(page).to have_selector('h2', text: @help_section1.title)
    expect(page).to have_selector('li', text: @help_section1.title)
    click_link 'Edit'
    fill_in('help_section_title', with: nil, fill_options: { clear: :backspace })
    fill_in_trix_editor 'help_section_content_trix_input_help_section_' + @help_section1.id.to_s, with: content
    click_button 'Submit'
    expect(page).to have_content('1 error prohibited this help section from being saved:')
    expect(page).to have_content("Title can't be blank")
  end

  scenario 'and deletes it', js: true do
    sign_in @admin
    visit help_path
    expect(page).to have_selector('h2', text: @help_section1.title)
    expect(page).to have_selector('li', text: @help_section1.title)
    click_link 'Edit'
    click_button 'Delete Help Section'
    page.accept_alert
    expect(page).to have_content('Help section was successfully deleted.')
  end
end
