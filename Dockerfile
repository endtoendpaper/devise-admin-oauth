ARG RUBY_VERSION='3.4.1'

######################## BASE STAGE ########################
FROM ruby:$RUBY_VERSION AS base

RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
    apt-get update -yqq && apt-get install -yqq \
    cron \
    libglib2.0-0 \
    libglib2.0-dev \
    libheif-dev \
    libpoppler-glib8 \
    libvips \
    libvips-dev \
    netcat-openbsd \
    nodejs \
    npm \
    postgresql-client \
    vim \
    yarn

WORKDIR /app

COPY Gemfile* /app/

ARG BUNDLER_VERSION='2.5.23'
RUN gem install bundler -v $BUNDLER_VERSION
ARG BUNDLE_COMMAND="bundle _${BUNDLER_VERSION}_ install"
RUN $BUNDLE_COMMAND
RUN yarn install

COPY . /app/

RUN npm install -g esbuild sass

COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh

EXPOSE 3000

ENTRYPOINT ["entrypoint.sh"]

######################## DEVELOPMENT STAGE ########################
FROM base AS development

CMD ["./bin/dev"]

######################## DEPLOY STAGE ########################
FROM base AS deploy

RUN chmod +x /app/wait-for

RUN ./bin/rails assets:clean assets:precompile

CMD ["bundle", "exec", "puma", "-C", "config/puma.rb"]
