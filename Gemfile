source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby "3.4.1"

gem 'rails', '~> 8.0', '>= 8.0.1'

gem 'sprockets-rails', '~> 3.5', '>= 3.5.2'
gem 'pg', '~> 1.5', '>= 1.5.9'
gem 'puma', '~> 6.6'
gem 'jsbundling-rails', '~> 1.3', '>= 1.3.1'
gem 'turbo-rails', '~> 2.0', '>= 2.0.11'
gem 'stimulus-rails', '~> 1.3', '>= 1.3.4'
gem 'cssbundling-rails', '~> 1.4', '>= 1.4.1'
gem 'jbuilder', '~> 2.13'
gem 'tzinfo-data', '~> 1.2025', '>= 1.2025.1', platforms: %i[ mingw mswin x64_mingw jruby ]
gem 'bootsnap', '~> 1.18', '>= 1.18.4', require: false
gem 'haml-rails', '~> 2.1'
gem 'csv', '~> 3.3', '>= 3.3.2'
gem 'dotenv-rails', '~> 3.1', '>= 3.1.7'
gem 'redis', '~> 5.3'
gem 'sidekiq', '~> 7.3', '>= 7.3.8'
gem 'image_processing', '~> 1.13'
gem 'aws-sdk', '~> 3.2'
gem 'activestorage-validator', '~> 0.4.0'
gem 'will_paginate', '~> 4.0', '>= 4.0.1'
gem 'will_paginate-bootstrap-style', '~> 0.3.0'
gem 'devise', '~> 4.9', '>= 4.9.4'
gem 'omniauth', '~> 2.1', '>= 2.1.2'
gem 'omniauth-google-oauth2', '~> 1.2', '>= 1.2.1'
gem 'omniauth-rails_csrf_protection', '~> 1.0', '>= 1.0.2'
gem 'whenever', '~> 1.0'
gem 'local_time', '~> 3.0', '>= 3.0.2'
gem 'elasticsearch', '~> 8.16'
gem 'elasticsearch-rails', '~> 8.0'
gem 'elasticsearch-model', '~> 8.0'

group :development, :test do
  gem 'debug', '~> 1.10', platforms: %i[ mri mingw x64_mingw ]
  gem 'factory_bot_rails', '~> 6.4', '>= 6.4.4'
  gem 'faker', '~> 3.5', '>= 3.5.1'
end

group :development do
  gem 'web-console', '~> 4.2', '>= 4.2.1'
  gem 'devise_masquerade', '~> 2.1', '>= 2.1.3'
end

group :test do
  gem 'capybara', '~> 3.40'
  gem 'selenium-webdriver', '~> 4.28'
  gem 'rspec-rails', '~> 7.1'
  gem 'rails-controller-testing', '~> 1.0', '>= 1.0.5'
  gem 'database_cleaner-active_record', '~> 2.2'
  gem 'rspec-sidekiq', '~> 5.0'
end
