unless Rails.env.test?
  Sidekiq.configure_server do |config|
    config.redis = {url: ENV["REDIS_URL"] }
  end

  Sidekiq.configure_client do |config|
    config.redis = {url: ENV["REDIS_URL"] }
  end
end

if Rails.env.test?
  Sidekiq::Testing.inline!
end
