require 'sidekiq/web'

Rails.application.routes.draw do
  root "static_pages#home"
  get '/404', to: 'errors#not_found'
  get  "/about", to: "static_pages#about"
  get  "/help", to: "static_pages#help"
  get  "/user-management", to: "static_pages#user_management"
  resources :help_sections, only: [:new, :create, :edit, :update, :destroy], :path => "documentation"
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks', registrations: "registrations" }
  devise_scope :user do
    delete "delete_avatar" => "registrations"
  end
  authenticate :user, lambda { |u| u.admin? } do
    mount Sidekiq::Web => '/sidekiq'
  end
  resources :support_issues, only: [:index, :show, :new, :create, :update], :path => "support" do
    resources :support_replies, only: [:create], :path => "reply"
    resources :support_comments, only: [:create], :path => "comment"
    resources :support_notes, only: [:create], :path => "note"
  end
  resources :users, only: [:index, :show, :new, :edit, :update, :destroy] do
    member do
      delete :delete_avatar
      resources :support_issues, only: [:index], :path => "support"
      get :following, :followers
    end
  end
  resources :site_settings, only: [:edit, :update], :path => "settings" do
    member do
      delete :delete_default_avatar
      delete :delete_logo
      delete :delete_favicon
      delete :delete_apple_touch_icon
    end
  end
  resources :relationships, only: [:create, :destroy]
  resources :tables, only: [:show, :create, :update]
end
