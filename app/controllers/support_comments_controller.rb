class SupportCommentsController < ApplicationController
  before_action :authenticate_user!
  before_action :users_allowed_to_contact
  before_action :set_support_issue
  before_action :correct_user
  before_action :open_issue

  def create
    @support_comment = @support_issue.support_comments.build(support_comment_params)
    respond_to do |format|
      if @support_comment.save
        @support_comment.update(user_email: current_user.email)
        @support_comment.update(user_id: current_user.id)
        if SiteSetting.email_admins_contact_form?
          AdminsMailer.support_comment(@support_comment, current_user).deliver_later
        end
        format.html { redirect_to support_issue_path(@support_issue),
                                  notice: 'Comment successfully submitted.' }
        format.json { redirect_to support_issue_path(@support_issue),
                      status: :created, location: @support_issue }
      else
        format.html { redirect_to support_issue_path(@support_issue), status: :unprocessable_entity }
        format.json { render json: @support_comment.errors, status: :unprocessable_entity }
        format.turbo_stream { render :form_update_for_issue_comment,
          status: :unprocessable_entity }
      end
    end
  end

  private

  def open_issue
    redirect_to support_issue_path(@support_issue), alert: 'Issue has already been resolved.' unless @support_issue.addressed != true
  end

  def users_allowed_to_contact
    render 'errors/not_found' unless SiteSetting.first.contact_form
  end

  def correct_user
    redirect_to root_path, alert: 'Not authorized.' unless (@support_issue.user_id == current_user.id)
  end

  def set_support_issue
    @support_issue = SupportIssue.find(params[:support_issue_id].delete_prefix("ISSUE-"))
  end

  def support_comment_params
    params.require(:support_comment).permit(:body, :rich_text_body)
  end
end
