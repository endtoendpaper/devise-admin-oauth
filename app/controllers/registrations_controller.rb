class RegistrationsController < Devise::RegistrationsController
  before_action :require_no_authentication, only: %i[new cancel]
  before_action :correct_user_for_create, only: [:create]
  before_action :correct_user_for_edit, only: [:delete_avatar]

  # GET /resource/sign_up
  def new
    return super if SiteSetting.allow_users_to_join?

    respond_to do |format|
      format.html { redirect_to root_path }
      format.json { head :ok, status: :unprocessable_entity }
    end
  end

  # POST /resource
  def create
    return super unless current_user

    build_resource(sign_up_params)
    resource.save
    yield resource if block_given?
    if resource.persisted?
      if resource.active_for_authentication?
        set_flash_message! :notice, :created
        sign_up(resource_name, resource)
        redirect_to resource
      else
        set_flash_message! :notice, :"created_but_#{resource.inactive_message}"
        expire_data_after_sign_in!
        redirect_to resource
      end
    else
      clean_up_passwords resource
      set_minimum_password_length
      alert_string = @user.errors.full_messages.join(', ')
      respond_to do |format|
        format.html { redirect_to new_user_path, alert: alert_string }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def delete_avatar
    @user = current_user
    @user.avatar.purge if @user.avatar.attached?
    respond_to do |format|
      format.html { redirect_to users_path }
      format.turbo_stream
    end
  end

  protected

  def update_resource(resource, params)
    # Require current password if user is trying to change password.
    return super if params['password']&.present?

    # Allows user to update registration information without password.
    resource.update_without_password(params.except('current_password'))
  end

  private

  def format_errors(errors)
    puts errors.join(', ')
  end

  def correct_user_for_create
    if (logged_in_not_admin? || (!current_user && !registrations_allowed?))
      redirect_to root_path, alert: 'Not authorized.'
    end
  end

  def logged_in_not_admin?
    return (current_user && !current_user.admin?)
  end

  def registrations_allowed?
    return (SiteSetting.allow_users_to_join?)
  end

  def correct_user_for_edit
    if !current_user
      redirect_to root_path, alert: 'Not authorized.'
    end
  end
end
