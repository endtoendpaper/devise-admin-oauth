class SiteSettingsController < ApplicationController
  before_action :authenticate_user!
  before_action :correct_user
  before_action :set_object

  # GET /users/1/edit
  def edit; end

  # PATCH/PUT /users/1 or /users/1.json
  def update
    respond_to do |format|
      if @siteSetting.update(object_params)
        format.html do
          redirect_to edit_site_setting_path(@siteSetting),
                      notice: 'Site settings were successfully updated.'
        end
        format.json { render :show, status: :ok, location: root_path }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @siteSetting.errors,
                             status: :unprocessable_entity }
      end
    end
  end

  def delete_default_avatar
    @siteSetting.default_avatar.purge if @siteSetting.default_avatar.attached?
    default_avatar_path = Rails.root.join('public/default_avatar', "default*" ).to_s
    FileUtils.rm_rf(Dir[default_avatar_path])
    respond_to do |format|
      format.html { redirect_to edit_site_setting_path(@siteSetting) }
      format.turbo_stream
    end
  end

  def delete_logo
    @siteSetting.logo.purge if @siteSetting.logo.attached?
    logo_path = Rails.root.join('public/logo', "logo*" ).to_s
    FileUtils.rm_rf(Dir[logo_path])
    respond_to do |format|
      format.html { redirect_to edit_site_setting_path(@siteSetting) }
      format.turbo_stream
    end
  end

  def delete_favicon
    @siteSetting.favicon.purge if @siteSetting.favicon.attached?
    favicon_path = Rails.root.join('public/favicon', "favicon*" ).to_s
    FileUtils.rm_rf(Dir[favicon_path])
    respond_to do |format|
      format.html { redirect_to edit_site_setting_path(@siteSetting) }
      format.turbo_stream
    end
  end

  def delete_apple_touch_icon
    @siteSetting.apple_touch_icon.purge if @siteSetting.apple_touch_icon.attached?
    apple_touch_icon_path = Rails.root.join('public/apple_touch_icon', "apple*" ).to_s
    FileUtils.rm_rf(Dir[apple_touch_icon_path])
    respond_to do |format|
      format.html { redirect_to edit_site_setting_path(@siteSetting) }
      format.turbo_stream
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_object
      @siteSetting = SiteSetting.first
    end

    # Only allow a list of trusted parameters through.
    def object_params
      params.require(:site_setting).permit(:default_avatar, :users_can_join,
                                :site_name, :logo, :favicon, :apple_touch_icon,
                                :display_site_name,
                                :contact_form, :rich_text_support_replies,
                                :rich_text_support_issues,
                                :email_issue_status_to_users,
                                :email_contact_form_to_admins, :follow_users)
    end

    def correct_user
      redirect_to root_path, alert: 'Not authorized.' unless current_user.admin?
    end
end
