class UsersController < ApplicationController
  before_action :set_user, only: %i[show edit update destroy delete_avatar followers following]
  before_action :authenticate_user!, except: [:show, :index, :following, :followers]
  before_action :follow_enabled, only: [:following, :followers]
  before_action :hide_followers, only: [:following, :followers]
  before_action :admin_user, except: [:show, :index, :following, :followers]

  # GET /users or /users.json
  def index
    @users = User.search(params[:search], page: params[:page], per_page: 5)
  end

  # GET /users/1 or /users/1.json
  def show; end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit; end

  # PATCH/PUT /users/1 or /users/1.json
  def update
    respond_to do |format|
      if @user.update_without_password(user_params)
        format.html { redirect_to user_path(@user),
                      notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: root_path }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1 or /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_path,
                                notice: 'User was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  def delete_avatar
    @user.avatar.purge if @user.avatar.attached?
    respond_to do |format|
      format.html { redirect_to edit_user_path(@user) }
      format.turbo_stream
    end
  end

  def following
    @title = "Following"
    @users = @user.following.paginate(page: params[:page], per_page: 5)
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    if current_user && current_user.admin
      @users = @user.followers.paginate(page: params[:page], per_page: 5)
    else
      @users = @user.followers.where(hide_relationships: false).paginate(page: params[:page], per_page: 5)
    end
    render 'show_follow'
  end

  private

  def follow_enabled
    render 'errors/not_found' unless SiteSetting.first.follow_users
  end

  def hide_followers
    if @user.hide_relationships && (!current_user || (!current_user.admin && current_user.id != @user.id))
      render 'errors/not_found'
    end
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user =User.find_by(username: params[:id])
    render 'errors/not_found' unless @user
  end

  # Only allow a list of trusted parameters through.
  def user_params
    params.require(:user).permit(:avatar, :password, :password_confirmation,
                                 :email, :username, :admin, :darkmode,
                                 :time_zone, :hide_relationships)
  end

  def admin_user
    redirect_to root_path, alert: 'Not authorized.' unless current_user.admin?
  end
end
