class ApplicationController < ActionController::Base
  helper ActionTextHelper
  helper CssClassHelper

  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  if Rails.env.development?
    before_action :masquerade_user!
  end

  protected

  def configure_permitted_parameters
    if current_user and current_user.admin?
      devise_parameter_sanitizer.permit(:sign_up,
                                      keys: %i[username admin email password
                                               password_confirmation])
      devise_parameter_sanitizer.permit(:account_update,
      keys: %i[avatar username email admin
                darkmode time_zone password_confirmation
                current_password hide_relationships])
    else
      devise_parameter_sanitizer.permit(:sign_up,
                                      keys: %i[username email password
                                                password_confirmation])
      devise_parameter_sanitizer.permit(:account_update,
                  keys: %i[avatar username email hide_relationships
                            darkmode time_zone password_confirmation
                            current_password])
      devise_parameter_sanitizer.permit(:sign_in,
                keys: %i[login password])
    end
  end
end
