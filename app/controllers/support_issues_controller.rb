class SupportIssuesController < ApplicationController
  before_action :authenticate_user!
  before_action :users_allowed_to_contact
  before_action :is_admin, only: [:update]
  before_action :set_issue, only: %i[show update]
  before_action :correct_user_for_index, only: [:index]
  before_action :correct_user_for_show, only: [:show]

  def index
    require 'will_paginate/array'
    if params[:id]
      @user = User.find_by(username: params[:id])
      @support_issues = @user.support_issues.order(addressed: :asc, updated_at: :desc )
      @support_issues = @support_issues.paginate(page: params[:page], per_page: 5)
    else
      @support_issues = SupportIssue.all.order(addressed: :asc, updated_at: :desc )
      @support_issues = @support_issues.paginate(page: params[:page], per_page: 5)
    end
  end

  def show
    require 'will_paginate/array'
    @comments = @support_issue.support_comments
    @replies = @support_issue.support_replies
    if current_user.admin
      @notes = @support_issue.support_notes
      @all_comments = @comments + @replies + @notes
      @all_comments = @all_comments.sort_by(&:created_at)
    else
      @all_comments = @comments + @replies
      @all_comments = @all_comments.sort_by(&:created_at)
    end
    if current_user.id == @support_issue.user_id
      @support_comment = @support_issue.support_comments.build
    else
      @support_note = @support_issue.support_notes.build
      @support_reply = @support_issue.support_replies.build
    end
  end

  def new
    @support_issue = current_user.support_issues.build
  end

  def create
    @support_issue = current_user.support_issues.build(support_issue_create_params)
    respond_to do |format|
      if @support_issue.save
        @support_issue.update(user_id: current_user.id)
        @support_issue.update(user_email: current_user.email)
        @support_issue.update(status_log:
          'Issue Created at ' +
          @support_issue.created_at.strftime('%b %d, %Y %I:%M%p %Z') +
          ' by ' + current_user.username + ' (' +
          current_user.email + ")\n")
        if SiteSetting.email_admins_contact_form?
          AdminsMailer.support_issue_created(@support_issue, current_user).deliver_later
        end
        if SiteSetting.email_issue_status_to_users?
          UserMailer.support_issue_created(@support_issue).deliver_later
        end
        format.html { redirect_to help_url,
                                  notice: 'Support form successfully submitted.' }
        format.json { render :show, status: :created, location: @support_issue }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @support_issue.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @support_issue.update(support_issue_update_params)
        if @support_issue.addressed == true
          update_log = @support_issue.status_log + 'Issue Resolved at ' + @support_issue.updated_at.strftime('%b %d, %Y %I:%M%p %Z') + ' by ' + current_user.username + ' (' + current_user.email + ")\n"
          @support_issue.update(status_log: update_log)
          if SiteSetting.email_admins_contact_form?
            AdminsMailer.support_issue_resolved(@support_issue, current_user).deliver_later
          end
          if SiteSetting.email_issue_status_to_users?
            UserMailer.support_issue_resolved(@support_issue).deliver_later
          end
        else
          update_log = @support_issue.status_log + 'Issue Reopened at ' + @support_issue.updated_at.strftime('%b %d, %Y %I:%M%p %Z') + ' by ' + current_user.username + ' (' + current_user.email + ")\n"
          @support_issue.update(status_log: update_log)
          if SiteSetting.email_admins_contact_form?
            AdminsMailer.support_issue_reopened(@support_issue, current_user).deliver_later
          end
          if SiteSetting.email_issue_status_to_users?
            UserMailer.support_issue_reopened(@support_issue).deliver_later
          end
        end
        format.html { redirect_to @support_issue,
                                  notice: 'Support issue status has been updated.' }
        format.json { render :show, status: :ok, location: @support_issue }
      else
        format.html { redirect_to @support_issue, status: :unprocessable_entity }
        format.json { render json: @support_issue.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_issue
    @support_issue = SupportIssue.find_by_id(params[:id].delete_prefix("ISSUE-"))
    render 'errors/not_found' unless @support_issue
  end

  # Only admins can view support issues show and index
  def is_admin
    redirect_to root_path unless current_user.admin?
  end

  def correct_user_for_index
    if params[:id]
      render 'errors/not_found' unless (params[:id] == current_user.username || current_user.admin?)
    else
      render 'errors/not_found' unless current_user.admin?
    end
  end

  def correct_user_for_show
    render 'errors/not_found' unless (@support_issue.user_id == current_user.id || current_user.admin?)
  end

  def users_allowed_to_contact
    render 'errors/not_found' unless SiteSetting.first.contact_form
  end

  # Only allow a list of trusted parameters through.
  def support_issue_create_params
    params.require(:support_issue).permit(:summary, :body, :rich_text_body)
  end

  def support_issue_update_params
    params.require(:support_issue).permit(:addressed)
  end
end
