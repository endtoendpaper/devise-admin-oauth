class SupportNotesController < ApplicationController
  before_action :authenticate_user!
  before_action :users_allowed_to_contact
  before_action :set_support_issue
  before_action :correct_user

  def create
    @support_note = @support_issue.support_notes.build(support_note_params)
    respond_to do |format|
      if @support_note.save
        @support_note.update(user_email: current_user.email)
        @support_note.update(user_id: current_user.id)
        if SiteSetting.email_admins_contact_form?
          AdminsMailer.support_note(@support_note, current_user).deliver_later
        end
        format.html { redirect_to support_issue_path(@support_issue),
                                  notice: 'Internal note successfully submitted.' }
        format.json { redirect_to support_issue_path(@support_issue),
                      status: :created, location: @support_issue }
      else
        format.html { redirect_to support_issue_path(@support_issue), status: :unprocessable_entity }
        format.json { render json: @support_note.errors, status: :unprocessable_entity }
        format.turbo_stream { render :form_update_for_issue_note,
          status: :unprocessable_entity }
      end
    end
  end

  private

  def users_allowed_to_contact
    render 'errors/not_found' unless SiteSetting.first.contact_form
  end

  def correct_user
    redirect_to root_path, alert: 'Not authorized.' unless (@support_issue.user_id != current_user.id && current_user.admin?)
  end

  def set_support_issue
    @support_issue = SupportIssue.find(params[:support_issue_id].delete_prefix("ISSUE-"))
  end

  def support_note_params
    params.require(:support_note).permit(:body, :rich_text_body)
  end

end
