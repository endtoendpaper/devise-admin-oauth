class StaticPagesController < ApplicationController
  before_action :authenticate_user!, only: [:help, :user_management]
  before_action :admin_user, only: [:user_management]

  def home; end

  def about; end

  def help
    @help_sections = HelpSection.order('lower(title)').all
  end

  def user_management
    if params[:view_admins].present? && params[:view_admins] == '1'
      @users = User.admin_search(params[:search], admin: true, page: params[:page], per_page: 2)
    else
      @users = User.admin_search(params[:search], page: params[:page], per_page: 2)
    end
  end

  private

  def admin_user
    redirect_to root_path, alert: 'Not authorized.' unless current_user.admin?
  end
end
