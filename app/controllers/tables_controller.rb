class TablesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_table, only: [:show, :update]
  before_action :set_record, only: [:show, :update]
  before_action :has_create_privileges, only: [:create]
  before_action :has_edit_privileges, only: [:show, :update]

  def show
    render json: {
      sgid: @table.attachable_sgid,
      content: render_to_string(partial: "tables/editor", locals: { table: @table }, formats: [:html])
    }
  end

  def create
    @table = Table.create(record_type: params[:record_type], record_id: params[:record_id])
    render json: {
      sgid: @table.attachable_sgid,
      content: render_to_string(partial: "tables/editor", locals: { table: @table }, formats: [:html])
    }
  end

  def update
    if params["method"] == "addRow"
      @table.rows += 1
    elsif params["method"] == "addColumn"
      @table.columns += 1
    elsif params["method"] == "removeRow"
      @table.rows -= 1
    elsif params["method"] == "removeColumn"
      @table.columns -= 1
    elsif params["method"] == "updateCell"
      @table.data[params['cell']] = params['value']
    elsif params["method"] == "headerRow"
      @table.header_row = params['header_row']
    end
    @table.save
    render json: {
      sgid: @table.attachable_sgid,
      content: render_to_string(partial: "tables/editor", locals: { table: @table }, formats: [:html])
    }
  end

  private

    def set_table
      @table = ActionText::Attachable.from_attachable_sgid params[:id]
    end

    def set_record
      @record = @table.record
    end

    def has_create_privileges
      if params[:record_type].present? && params[:record_id].present?
        record = params[:record_type].constantize.find(params[:record_id])
        redirect_to root_path, alert: 'Not authorized.' unless record.has_edit_privileges(current_user)
      end
    end

    def has_edit_privileges
      if @record
        redirect_to root_path, alert: 'Not authorized.' unless @record.has_edit_privileges(current_user)
      end
    end
end
