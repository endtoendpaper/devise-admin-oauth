class SupportRepliesController < ApplicationController
  before_action :authenticate_user!
  before_action :users_allowed_to_contact
  before_action :set_support_issue
  before_action :correct_user
  before_action :active_user
  before_action :open_issue

  def create
    @support_reply = @support_issue.support_replies.build(support_reply_params)
    respond_to do |format|
      if @support_reply.save
        @support_reply.update(user_email: current_user.email)
        @support_reply.update(user_id: current_user.id)
        if SiteSetting.email_admins_contact_form?
          AdminsMailer.support_reply(@support_reply, current_user).deliver_later
        end
        UserMailer.support_reply(@support_reply).deliver_later
        format.html { redirect_to support_issue_path(@support_issue),
                                  notice: 'User reply successfully submitted.' }
        format.json { redirect_to support_issue_path(@support_issue),
                      status: :created, location: @support_issue }
      else
        format.html { redirect_to support_issue_path(@support_issue), status: :unprocessable_entity }
        format.json { render json: @support_reply.errors, status: :unprocessable_entity }
        format.turbo_stream { render :form_update_for_issue_reply,
          status: :unprocessable_entity }
      end
    end
  end

  private

  def open_issue
    redirect_to support_issue_path(@support_issue), alert: 'Issue has already been resolved.' unless @support_issue.addressed != true
  end

  def active_user
    redirect_to support_issue_path(@support_issue), alert: 'User who created support issue is inactive.' unless !User.where(id: @support_issue.user_id).empty?
  end

  def users_allowed_to_contact
    render 'errors/not_found' unless SiteSetting.first.contact_form
  end

  def correct_user
    redirect_to root_path, alert: 'Not authorized.' unless (@support_issue.user_id != current_user.id && current_user.admin?)
  end

  def set_support_issue
    @support_issue = SupportIssue.find(params[:support_issue_id].delete_prefix("ISSUE-"))
  end

  def support_reply_params
    params.require(:support_reply).permit(:body, :rich_text_body)
  end
end
