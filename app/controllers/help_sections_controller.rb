class HelpSectionsController < ApplicationController
  before_action :authenticate_user!
  before_action :is_admin
  before_action :set_help_section, only: %i[edit update destroy]

  def new
    @help_section = HelpSection.new
  end

  def create
    @help_section = HelpSection.new(help_section_params)

    respond_to do |format|
      if @help_section.save
        format.html { redirect_to help_url,
                                  notice: 'Help section was successfully created.' }
                                  format.json { head :no_content }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @help_section.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
  end

  def update
    respond_to do |format|
      if @help_section.update(help_section_params)
        format.html { redirect_to help_url,
                                  notice: 'Help section was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @help_section.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @help_section.destroy

    respond_to do |format|
      format.html { redirect_to help_url,
                                notice: 'Help section was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private

  def is_admin
    redirect_to root_path, alert: 'Not authorized.' unless current_user.admin?
  end

  def set_help_section
    @help_section = HelpSection.find(params[:id])
  end

  def help_section_params
    params.require(:help_section).permit(:title, :content)
  end
end
