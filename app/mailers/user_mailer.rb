class UserMailer < ApplicationMailer
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.support_reply.subject
  #
  def support_reply(support_reply)
    @support_reply = support_reply
    @support_issue = SupportIssue.find(@support_reply.support_issue_id)
    add_attachments
    if !User.where(id: @support_issue.user_id).empty?
      @user = User.find(@support_issue.user_id)
      @user_email = @user.email
      mail(to: @user_email, subject: SiteSetting.site_name + ' ' + @support_issue.display_name + ': ' + @support_issue.summary)
    end
  end

  def support_issue_created(support_issue)
    @support_issue = support_issue
    if !User.where(id: @support_issue.user_id).empty?
      @user = User.find(@support_issue.user_id)
      @user_email = @user.email
      mail(to: @user_email, subject: SiteSetting.site_name + ' ' + @support_issue.display_name + ': ' + @support_issue.summary)
    end
  end

  def support_issue_reopened(support_issue)
    @support_issue = support_issue
    if !User.where(id: @support_issue.user_id).empty?
      @user = User.find(@support_issue.user_id)
      @user_email = @user.email
      mail(to: @user_email, subject: SiteSetting.site_name + ' ' + @support_issue.display_name + ': ' + @support_issue.summary)
    end
  end

  def support_issue_resolved(support_issue)
    @support_issue = support_issue
    if !User.where(id: @support_issue.user_id).empty?
      @user = User.find(@support_issue.user_id)
      @user_email = @user.email
      mail(to: @user_email, subject: SiteSetting.site_name + ' ' + @support_issue.display_name + ': ' + @support_issue.summary)
    end
  end

  private

  def add_attachments
    @support_reply.rich_text_body.embeds_attachments.each do |file|
      if attachment_size_ok(file) && attachment_extension_ok(file)
        attachments[file.blob.filename.to_s] = {
          mime_type: file.blob.content_type,
          content: file.blob.download
        }
      end
    end
  end

  def attachment_size_ok(file)
    # Less than 19 MB for google mail server
    ((file.byte_size.to_f / 1000000).round(2) < 19)
  end

  def attachment_extension_ok(file)
    # Don't send files with extensions google blocks or zip/gzip files
    blocked_formats = ['.ade', '.adp', '.apk', '.appx', '.appxbundle', '.bat',
      '.cab', '.chm', '.cmd', '.com', '.cpl', '.diagcab', '.diagcfg',
      '.diagpack', '.dll', '.dmg', '.ex', '.ex_', '.exe', '.hta', '.img',
      '.ins', '.iso', '.isp', '.jar', '.jnlp', '.js', '.jse', '.lib', '.lnk',
      '.mde', '.msc', '.msi', '.msix', '.msixbundle', '.msp', '.mst', '.nsh',
      '.pif', '.ps1', '.scr', '.sct', '.shb', '.sys', '.vb', '.vbe', '.vbs',
      '.vhd', '.vxd', '.wsc', '.wsf', '.wsh', '.xll', '.zip', '.gzip']
    !blocked_formats.include? File.extname(file.filename.to_s)
  end
end
