// Entry point for the build script in your package.json
import "@hotwired/turbo-rails"
import * as bootstrap from "bootstrap"
import "trix"
import "@rails/actiontext"
import "./controllers"
import "./custom/image_upload"
import LocalTime from "local-time"

LocalTime.start()

// Alerts
var alertList = document.querySelectorAll('.alert')
var alerts =  [].slice.call(alertList).map(function (element) {
  return new bootstrap.Alert(element)
});
