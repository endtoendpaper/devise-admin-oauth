module UsersHelper
  def user_left_col_classes
    if action_name == 'following' || action_name == 'followers'
      " col-xl-3 "
    else
      " col-sm-6 col-md-4 col-lg-3 col-xl-3 col-xxl-2 mt-3 "
    end
  end

  def user_right_col_classes
    if action_name == 'following' || action_name == 'followers'
      " col-xl-9 "
    else
      " col-sm-6 col-md-8 col-lg-9 col-xl-9 col-xxl-10 "
    end
  end

  def can_view_follower_stats(user)
    !user.hide_relationships || (current_user && (current_user.admin || current_user.id == user.id))
  end
end
