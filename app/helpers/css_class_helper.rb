module CssClassHelper
  def active_link_class(path)
    if current_page?(path)
      ' active'
    else
      ''
    end
  end

  def navbar_class
    if user_signed_in? and current_user.darkmode
      ' bg-primary '
    else
      ' bg-primary '
    end
  end

  def dropdown_class
    if user_signed_in? and current_user.darkmode
      ' dropdown-menu-dark '
    else
      ' '
    end
  end

  def link_class
    if user_signed_in? and current_user.darkmode
      ' link-light'
    else
      ''
    end
  end

  def input_class
    if user_signed_in? and current_user.darkmode
      ' text-white bg-dark'
    else
      ''
    end
  end

  def light_text_class
    if user_signed_in? and current_user.darkmode
      ' text-white'
    else
      ''
    end
  end

  def table_class
    if user_signed_in? and current_user.darkmode
      ' table-dark'
    else
      ''
    end
  end

  def status_button_class
    if user_signed_in? and current_user.darkmode
      ' btn-outline-light '
    else
      ' btn-outline-primary '
    end
  end

  def follow_button_class
    if user_signed_in? and current_user.darkmode
      ' btn-outline-light '
    else
      ' btn-outline-primary '
    end
  end

  def unfollow_button_class
    if user_signed_in? and current_user.darkmode
      ' btn-outline-light '
    else
      ' btn-outline-dark '
    end
  end

  def masquerade_button_class
    if user_signed_in? and current_user.darkmode
      ' btn-outline-light '
    else
      ' btn-outline-secondary '
    end
  end

  def list_group_item_class
    if user_signed_in? and current_user.darkmode
      ' list-group-item-dark '
    else
      ' list-group-item-secondary '
    end
  end

  def outline_button_class
    if user_signed_in? and current_user.darkmode
      ' btn-outline-light '
    else
      ' btn-outline-primary '
    end
  end
end
