module ActionTextHelper
  def truncate_actiontext_html(html, link_class: " ", max_length: 800, read_more_path: nil)
    # Parse the HTML fragment using Nokogiri
    doc = Nokogiri::HTML.fragment(html)

    # Traverse nodes and accumulate visible text up to the max length
    truncated_doc = Nokogiri::HTML.fragment('')
    current_length = 0
    node_truncated = false

    doc.children.each do |node|
      break if current_length >= max_length

      truncated_node = process_node_for_truncation(node, max_length - current_length)
      next unless truncated_node

      current_length += visible_text_length(truncated_node)
      truncated_doc.add_child(truncated_node)

      if truncated_node.to_html.size < node.to_html.size
        node_truncated = true
      end
    end

    # Check if truncation occurred
    if node_truncated || (current_length >= max_length && read_more_path)
      # Append "…read more" link
      read_more_link = Nokogiri::XML::Node.new("a", doc)
      read_more_link['href'] = read_more_path
      read_more_link['class'] = link_class
      read_more_link.content = '…read more'
      truncated_doc.add_child(read_more_link)
    elsif node_truncated || current_length >= max_length
      # Append ellipsis as a text node
      ellipsis_node = Nokogiri::XML::Text.new('…', doc)
      truncated_doc.add_child(ellipsis_node)
    end

    # Convert the truncated content back to HTML with attachments intact
    sanitized_html = truncated_doc.to_html
    helpers.sanitize(sanitized_html, tags: allowed_tags, attributes: allowed_attributes).html_safe
  end

  private

  # Use Rails helpers for sanitization
  def helpers
    ActionController::Base.helpers
  end

  # Recursively process nodes to truncate them correctly
  def process_node_for_truncation(node, remaining_length)
    if node.text? # Handle text nodes
      truncated_text = node.text[0, remaining_length]
      Nokogiri::XML::Text.new(truncated_text, node.document)
    elsif node.element? # Handle HTML elements (including attachments)
      if actiontext_attachment?(node)
        return node.dup # Preserve the entire attachment node
      end

      cloned_element = node.dup(1) # Duplicate the element for modification
      cloned_element.children.remove # Start fresh with no children

      node.children.each do |child|
        break if remaining_length <= 0

        truncated_child = process_node_for_truncation(child, remaining_length)
        if truncated_child
          remaining_length -= visible_text_length(truncated_child)
          cloned_element.add_child(truncated_child)
        end
      end

      cloned_element
    end
  end

  # Calculate visible text length (ignoring HTML tags)
  def visible_text_length(node)
    node.text.strip.length
  end

  # Detect ActionText attachment nodes
  def actiontext_attachment?(node)
    node.name == 'action-text-attachment' || node['class']&.include?('attachment')
  end

  # Allowed HTML tags
  def allowed_tags
    ActionView::Base.sanitized_allowed_tags + ['table','tr','td','th','tbody','thead',
    'strong','em','b','i','p','code','pre','tt','samp','kbd','var','sub','sup','dfn',
    'cite','big','small','address','hr','br','div','span','h1','h2','h3','h4','h5','h6',
    'ul','ol','li','dl','dt','dd','abbr','acronym','a','img','blockquote','del',
    'ins','action-text-attachment','figure','figcaption']
  end

  # Allowed HTML attributes
  def allowed_attributes
    ActionView::Base.sanitized_allowed_attributes + %w[
      sgid content-type filename filesize width height url class href src previewable presentation style
    ]
  end
end
