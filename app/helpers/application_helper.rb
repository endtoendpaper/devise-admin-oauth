module ApplicationHelper
  def full_title(title)
    if title.empty?
      SiteSetting.site_name
    else
      title + ' | ' + SiteSetting.site_name
    end
  end

  def img_asset_path(image, size)
    object = SiteSetting.send("#{image}")
    file_ext = File.extname(object.blob.filename.to_s)
    asset_path = "#{image}/#{image}-" + size.to_s + "-" + SiteSetting.updated_at.to_i.to_s + file_ext
    path = Rails.root.join('public', asset_path).to_s
    if File.exist?(path)
      "/" + asset_path
    else
      begin
        dir_path = Rails.root.join("public/#{image}", "#{image}-#{size.to_s}-*" ).to_s
        FileUtils.rm_rf(Dir[dir_path])
        object.variant(size).processed
        File.open(path, 'wb') do |file|
          file.write(object.variant(size).download)
        end
        "/" + asset_path
      rescue
        object.variant(size)
      end
    end
  end

  def logo_navbar_path
    img_asset_path("logo", :navbar)
  end

  def favicon_path(size)
    img_asset_path("favicon", size)
  end

  def apple_icon_path(size)
    img_asset_path("apple_touch_icon", size)
  end

  def default_avatar_path(size)
    img_asset_path("default_avatar", size)
  end

  def current_timezone
    if current_user
      current_user.time_zone
    end
  end

  def pretty_time(time)
    if current_timezone
      time.in_time_zone(current_timezone).strftime('%b %d, %Y %I:%M%p %Z') unless time.nil?
    else
      local_time(time, '%b %d, %Y %I:%M%p %Z') unless time.nil?
    end
  end

  def resource_created(resource)
    if current_timezone
      resource.created_at.in_time_zone(current_timezone).strftime('%b %d, %Y %I:%M%p %Z')
    else
      local_time(resource.created_at, '%b %d, %Y %I:%M%p %Z')
    end
  end

  def resource_updated(resource)
    if current_timezone
      resource.updated_at.in_time_zone(current_timezone).strftime('%b %d, %Y %I:%M%p %Z')
    else
      local_time(resource.updated_at, '%b %d, %Y %I:%M%p %Z')
    end
  end
end
