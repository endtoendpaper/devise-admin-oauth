class SupportNote < ApplicationRecord
  include Support
  include SupportAdminReplies

  belongs_to :support_issue
end
