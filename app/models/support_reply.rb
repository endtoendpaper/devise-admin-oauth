class SupportReply < ApplicationRecord
  include Support
  include SupportAdminReplies

  belongs_to :support_issue
end
