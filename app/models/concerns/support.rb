module Support
  extend ActiveSupport::Concern

  included do
    include RichTextExtensions

    belongs_to :user, optional: true
    has_rich_text :rich_text_body

    validate :rich_or_plain_text
    validates_length_of :body, minimum: 10, maximum: 2000, allow_blank: true
    validate :rich_text_length

    def creator
      user
    end

    def creator_handle
      '@' + creator.username unless creator.nil?
    end

    def has_edit_privileges user
      user && user.id == creator.id
    end
  end
end

module SupportAdminReplies
  extend ActiveSupport::Concern

  included do

    private

    def rich_or_plain_text
      if rich_text_body.blank? && body.blank?
        errors.add(:base, "Details can't blank")
      elsif !rich_text_body.blank? && !SiteSetting.first.rich_text_support_replies
        errors.add(:base, "Only plain text submissions")
      elsif !rich_text_body.blank? && !body.blank?
        errors.add(:base, "Only plain text or rich text")
      end
    end

    def rich_text_length
      if SiteSetting.first.rich_text_support_replies
        if ActionController::Base.helpers.strip_tags(self.rich_text_body.to_plain_text).length > 2000
          errors.add(:base, "Rich text body is too long (maximum is 2000 characters)")
        elsif ActionController::Base.helpers.strip_tags(self.rich_text_body.to_plain_text).length < 10
          errors.add(:base, "Rich text body is too short (minimum is 10 characters)")
        end
      end
    end
  end
end

module SupportUserReplies
  extend ActiveSupport::Concern

  included do
    private

    def rich_or_plain_text
      if rich_text_body.blank? && body.blank?
        errors.add(:base, "Details can't blank")
      elsif !rich_text_body.blank? && !SiteSetting.first.rich_text_support_issues
        errors.add(:base, "Only plain text submissions")
      elsif !rich_text_body.blank? && !body.blank?
        errors.add(:base, "Only plain text or rich text")
      end
    end

    def rich_text_length
      if SiteSetting.first.rich_text_support_issues && body.blank?
        if ActionController::Base.helpers.strip_tags(self.rich_text_body.to_plain_text).length > 2000
          errors.add(:base, "Rich text body is too long (maximum is 2000 characters)")
        elsif ActionController::Base.helpers.strip_tags(self.rich_text_body.to_plain_text).length < 10
          errors.add(:base, "Rich text body is too short (minimum is 10 characters)")
        end
      end
    end
  end
end

