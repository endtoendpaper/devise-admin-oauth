class SupportComment < ApplicationRecord
  include Support
  include SupportUserReplies

  belongs_to :support_issue
end
