class HelpSection < ApplicationRecord
  include EditNotice
  include RichTextExtensions

  has_rich_text :content

  validates :title, presence: true, length: { maximum: 100 }
  validates_presence_of :content

  default_scope { order('lower(help_sections.title)') }

  def has_edit_privileges user
    user && user.admin
  end
end
