require 'elasticsearch/model'

class User < ApplicationRecord
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  attr_writer :login

  devise :database_authenticatable, :registerable, :trackable,
         :confirmable, :lockable, :timeoutable,
         :recoverable, :rememberable, :validatable,
         :omniauthable, omniauth_providers: [:google_oauth2]

  if Rails.env.development?
    devise :masqueradable
  end

  has_one_attached :avatar do |attachable|
    attachable.variant :tiny, resize_to_limit: [15, 15]
    attachable.variant :thumb, resize_to_limit: [100, 100]
    attachable.variant :medium, resize_to_limit: [200, 200]
  end
  has_many :active_relationships, class_name:  "Relationship",
            foreign_key: "follower_id",
            dependent:   :destroy
  has_many :passive_relationships, class_name:  "Relationship",
            foreign_key: "followed_id",
            dependent:   :destroy
  has_many :following, through: :active_relationships, source: :followed
  has_many :followers, through: :passive_relationships, source: :follower
  has_many :support_issues, dependent: :nullify
  has_many :support_replies, dependent: :nullify
  has_many :support_comments, dependent: :nullify
  has_many :support_notes, dependent: :nullify

  VALID_EMAIL_REGEX = /\A[a-z\d\-_]*\z/i
  validates :username, presence: true, length: { maximum: 100 },
                    format: { with: VALID_EMAIL_REGEX, message: 'can only contain letters, numbers, dashes, or underscores' },
                    uniqueness: { case_sensitive: false }
  validates :avatar, blob: { content_type: :image, size_range: 1..(5.megabytes) }

  default_scope { order('lower(username)') }
  scope :ordered_by_id, -> { reorder(id: :asc) }
  scope :admins, -> { where(admin: true) }

  index_name "#{Rails.env}_users"

  settings do
    mappings dynamic: false do
      indexes :username, type: :text, analyzer: 'english'
      indexes :email, type: :keyword
      indexes :admin, type: :boolean
    end
  end

  def as_indexed_json(options = {})
    as_json(only: [:username, :email, :admin])
  end

  def to_param
    self.username_was
  end

  def self.from_omniauth(access_token)
    data = access_token.info
    user = User.where(email: data['email']).first

    # Create users if they don't exist
    if SiteSetting.allow_users_to_join? && !user
      email_username = data['email'].split("@").first
      email_username = email_username.gsub!(/\W/, '') if email_username.gsub!(/\W/, '').present?
      new_username = email_username
      inc = 1
      while User.where("lower(username) = ?", new_username.downcase).count > 0
        new_username = email_username + inc.to_s
        inc = inc + 1
      end
      user = User.new(username: new_username,
                         email: data['email'],
                         password: Devise.friendly_token[0, 20])
      user.skip_confirmation!
      user.save
    elsif user && !user.confirmed?
      user.skip_confirmation!
      user.save
    end

    user
  end

  def login
    @login || self.username || self.email
  end

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    login = conditions.delete(:login)
    if (login.include?('@'))
      where(conditions.to_h).where(["lower(email) = :value", { :value => login.downcase }]).first
    else
      where(conditions.to_h).where(["username = :value", { :value => login }]).first
    end
  end

  def self.search(query, page: 1, per_page: 50)
    page = page.to_i <= 0 ? 1 : page.to_i

    if query.present?
      search_body = {
        query: {
          multi_match: {
            query: query,
            fields: ['username'],
            fuzziness: "AUTO"
          }
        }
      }

      if per_page
        search_body.merge!({
          from: (page.to_i - 1) * per_page.to_i,
          size: per_page.to_i
        })
      end

      search_response = User.__elasticsearch__.search(search_body)

      WillPaginate::Collection.create(page.to_i, per_page.to_i, search_response.response['hits']['total']['value']) do |pager|
        pager.replace(search_response.records.to_a)
      end
    else
      all.order('username ASC').paginate(page: page, per_page: per_page)
    end
  end

  def self.admin_search(query, admin: false, page: 1, per_page: 50)
    page = page.to_i <= 0 ? 1 : page.to_i

    if query.present?
      search_body = {
        query: {
          bool: {
            must: [
              {
                multi_match: {
                  query: query,
                  fields: ['username^2', 'email'],
                  "fuzziness": "AUTO"
                }
              }
            ]
          }
        }
      }

      if admin
        search_body[:query][:bool][:filter] = [
          { term: { admin: true } }
        ]
      end

      if per_page
        search_body.merge!({
          from: (page.to_i - 1) * per_page.to_i,
          size: per_page.to_i
        })
      end

      search_response = User.__elasticsearch__.search(search_body)

      WillPaginate::Collection.create(page.to_i, per_page.to_i, search_response.response['hits']['total']['value']) do |pager|
        pager.replace(search_response.records.to_a)
      end
    elsif admin
      admins.order('username ASC').paginate(page: page, per_page: per_page)
    else
      all.order('username ASC').paginate(page: page, per_page: per_page)
    end
  end

  # Follows a user.
  def follow(other_user)
    following << other_user unless self == other_user || !SiteSetting.follow_users? || self.following?(other_user)
  end

  # Unfollows a user.
  def unfollow(other_user)
    following.delete(other_user) if self.following?(other_user) unless !SiteSetting.follow_users?
  end

  # Returns true if the current user is following the other user.
  def following?(other_user)
    following.include?(other_user)
  end

  def handle
    '@' + username
  end
end
