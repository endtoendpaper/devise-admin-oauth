class SiteSetting < ApplicationRecord
  has_one_attached :default_avatar do |attachable|
    attachable.variant :tiny, resize_to_limit: [25, 25]
    attachable.variant :thumb, resize_to_limit: [100, 100]
    attachable.variant :medium, resize_to_limit: [200, 200]
    attachable.variant :large, resize_to_limit: [400, 400]
  end
  has_one_attached :logo do |attachable|
    attachable.variant :navbar, resize_to_limit: [800, 100]
    attachable.variant :thumb, resize_to_limit: [100, 100]
  end
  has_one_attached :apple_touch_icon do |attachable|
    attachable.variant :thumb, resize_to_limit: [100, 100]
    attachable.variant :default, resize_to_limit: [180, 180]
    attachable.variant :one_hundred_forty_four, resize_to_limit: [144, 144]
    attachable.variant :one_hundred_fourteen, resize_to_limit: [114, 114]
    attachable.variant :seventy_two, resize_to_limit: [72, 72]
    attachable.variant :fifty_seven, resize_to_limit: [57, 57]
  end
  has_one_attached :favicon do |attachable|
    attachable.variant :thumb, resize_to_limit: [100, 100]
    attachable.variant :default, resize_to_limit: [32, 32]
    attachable.variant :large, resize_to_limit: [192, 192]
  end

  validates :site_name, length: { maximum: 50 }
  validates :default_avatar, blob: { content_type: :image, size_range: 1..(5.megabytes) }
  validates :apple_touch_icon, blob: { content_type: :image, size_range: 1..(5.megabytes) }
  validates :logo, blob: { content_type: :image, size_range: 1..(5.megabytes) }
  validates :favicon, blob: { content_type: :image, size_range: 1..(5.megabytes) }

  def self.id
    SiteSetting.first.id
  end

  def self.updated_at
    SiteSetting.first.updated_at
  end

  def self.site_name
    SiteSetting.first.site_name
  end

  def self.allow_users_to_join?
    SiteSetting.first.users_can_join
  end

  def self.default_avatar
    SiteSetting.first.default_avatar
  end

  def self.logo
    SiteSetting.first.logo
  end

  def self.favicon
    SiteSetting.first.favicon
  end

  def self.apple_touch_icon
    SiteSetting.first.apple_touch_icon
  end

  def self.display_site_name?
    SiteSetting.first.display_site_name
  end

  def self.allow_users_to_contact_admins?
    SiteSetting.first.contact_form
  end

  def self.email_admins_contact_form?
    SiteSetting.first.email_contact_form_to_admins
  end

  def self.rich_text_support_issues?
    SiteSetting.first.rich_text_support_issues
  end

  def self.rich_text_support_replies?
    SiteSetting.first.rich_text_support_replies
  end

  def self.email_issue_status_to_users?
    SiteSetting.first.email_issue_status_to_users
  end

  def self.follow_users?
    SiteSetting.first.follow_users
  end
end
