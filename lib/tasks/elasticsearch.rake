namespace :elasticsearch do
  desc "Create elasticsearch indices"
  task create_indices: :environment do
    User.__elasticsearch__.create_index!
    User.import
    User.__elasticsearch__.refresh_index!
  end
end
