# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[8.0].define(version: 2024_12_21_205408) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "pg_catalog.plpgsql"

  create_table "action_text_rich_texts", force: :cascade do |t|
    t.string "name", null: false
    t.text "body"
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "plain_text_body"
    t.index "to_tsvector('english'::regconfig, plain_text_body)", name: "tsvector_rich_text_idx", using: :gin
    t.index ["record_type", "record_id", "name"], name: "index_action_text_rich_texts_uniqueness", unique: true
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.string "service_name", null: false
    t.bigint "byte_size", null: false
    t.string "checksum"
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "help_sections", force: :cascade do |t|
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "relationships", force: :cascade do |t|
    t.bigint "follower_id"
    t.bigint "followed_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["followed_id"], name: "index_relationships_on_followed_id"
    t.index ["follower_id", "followed_id"], name: "index_relationships_on_follower_id_and_followed_id", unique: true
    t.index ["follower_id"], name: "index_relationships_on_follower_id"
  end

  create_table "site_settings", force: :cascade do |t|
    t.boolean "users_can_join", default: true
    t.string "site_name", default: "My New Site"
    t.boolean "display_site_name", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "contact_form", default: true
    t.boolean "rich_text_support_replies", default: true
    t.boolean "rich_text_support_issues", default: true
    t.boolean "email_contact_form_to_admins", default: true
    t.boolean "email_issue_status_to_users", default: true
    t.boolean "follow_users", default: true
  end

  create_table "support_comments", force: :cascade do |t|
    t.text "body"
    t.string "user_email"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "support_issue_id", null: false
    t.index ["support_issue_id"], name: "index_support_comments_on_support_issue_id"
    t.index ["user_id"], name: "index_support_comments_on_user_id"
  end

  create_table "support_issues", force: :cascade do |t|
    t.string "summary"
    t.text "body"
    t.boolean "addressed", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "user_email"
    t.bigint "user_id"
    t.text "status_log"
    t.index ["user_id"], name: "index_support_issues_on_user_id"
  end

  create_table "support_notes", force: :cascade do |t|
    t.text "body"
    t.string "user_email"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "support_issue_id", null: false
    t.index ["support_issue_id"], name: "index_support_notes_on_support_issue_id"
    t.index ["user_id"], name: "index_support_notes_on_user_id"
  end

  create_table "support_replies", force: :cascade do |t|
    t.text "body"
    t.bigint "support_issue_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "user_email"
    t.bigint "user_id"
    t.index ["support_issue_id"], name: "index_support_replies_on_support_issue_id"
    t.index ["user_id"], name: "index_support_replies_on_user_id"
  end

  create_table "tables", force: :cascade do |t|
    t.integer "columns", default: 1
    t.integer "rows", default: 1
    t.json "data", default: {}
    t.string "record_type"
    t.bigint "record_id"
    t.boolean "header_row", default: false
    t.boolean "record_saved", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["record_saved"], name: "index_tables_on_record_saved"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "username", default: "", null: false
    t.boolean "admin", default: false, null: false
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "darkmode", default: false
    t.string "time_zone", default: "UTC"
    t.boolean "hide_relationships", default: false
    t.index ["admin"], name: "index_users_on_admin"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["hide_relationships"], name: "index_users_on_hide_relationships"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "support_comments", "support_issues"
  add_foreign_key "support_comments", "users"
  add_foreign_key "support_issues", "users"
  add_foreign_key "support_notes", "support_issues"
  add_foreign_key "support_notes", "users"
  add_foreign_key "support_replies", "support_issues"
  add_foreign_key "support_replies", "users"
end
