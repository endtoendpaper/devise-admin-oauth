abort("The Rails environment is running in production mode!") if Rails.env.production?

User.where.not(id: User.ordered_by_id.first.id).destroy_all
SupportIssue.destroy_all
SupportReply.destroy_all
SupportNote.destroy_all
SupportComment.destroy_all
HelpSection.destroy_all

5.times do |i|
  FactoryBot.create(:help_section)
  FactoryBot.create(:user, :confirmed)
  FactoryBot.create(:user, :confirmed, :avatar)
end

4.times do |i|
  FactoryBot.create(:user)
end

admin_user = FactoryBot.create(:user, :confirmed, :admin).id
deleted_user = FactoryBot.create(:user, :confirmed).id
deleted_admin = FactoryBot.create(:user, :confirmed, :admin).id

8.times do |i|
  user = User.all[i+1]
  if i == 1
    admin = User.find(admin_user)
  else
    admin = User.find(deleted_admin)
  end

  if i.even?
    SiteSetting.first.update(rich_text_support_issues: 'true')
    SiteSetting.first.update(rich_text_support_replies: 'false')
    support_issue = FactoryBot.create(:support_issue, :addressed, :rich_text,
                                       user_id: user.id, user_email: user.email)
    support_issue.update(status_log:
            'Issue Created at ' +
            support_issue.created_at.strftime('%b %d, %Y %I:%M%p %Z') +
            ' by ' + user.username + ' (' +
            user.email + ")\r\n")
    support_issue.update(status_log: support_issue.status_log +
                  'Issue Resolved at ' +
                  support_issue.updated_at.strftime('%b %d, %Y %I:%M%p %Z') +
                  ' by ' + user.username + ' (' +
                  user.email + ")\r\n")
    FactoryBot.create(:support_reply, user_id: admin.id,
                      user_email: admin.email,
                      support_issue_id: support_issue.id)
    FactoryBot.create(:support_comment, :rich_text, user_id: user.id,
                      user_email: user.email,
                      support_issue_id: support_issue.id)
    FactoryBot.create(:support_note, user_id: admin.id, user_email: admin.email,
                      support_issue_id: support_issue.id)
    FactoryBot.create(:support_reply, user_id: admin.id,
                      user_email: admin.email,
                      support_issue_id: support_issue.id)
    support_issue2 = FactoryBot.create(:support_issue, :addressed, :rich_text,
                      user_id: user.id, user_email: user.email)
    support_issue2.update(status_log:
                        'Issue Created at ' +
                        support_issue2.created_at.strftime('%b %d, %Y %I:%M%p %Z') +
                        ' by ' + user.username + ' (' +
                        user.email + ")\r\n")
    support_issue2.update(status_log: support_issue2.status_log +
                        'Issue Resolved at ' +
                        support_issue2.updated_at.strftime('%b %d, %Y %I:%M%p %Z') +
                        ' by ' + user.username + ' (' +
                        user.email + ")\r\n")
  else
    SiteSetting.first.update(rich_text_support_issues: 'false')
    SiteSetting.first.update(rich_text_support_replies: 'true')
    support_issue = FactoryBot.create(:support_issue, user_id: user.id,
                                      user_email: user.email)
    support_issue.update(status_log:
                  'Issue Created at ' +
                  support_issue.created_at.strftime('%b %d, %Y %I:%M%p %Z') +
                  ' by ' + user.username + ' (' +
                  user.email + ")\r\n")
    FactoryBot.create(:support_reply, :rich_text, user_id: admin.id,
                      user_email: admin.email,
                      support_issue_id: support_issue.id)
    FactoryBot.create(:support_comment, user_id: user.id,
                      user_email: user.email,
                      support_issue_id: support_issue.id)
    FactoryBot.create(:support_note, :rich_text, user_id: admin.id,
                      user_email: admin.email,
                      support_issue_id: support_issue.id)
    FactoryBot.create(:support_reply, :rich_text, user_id: admin.id,
                      user_email: admin.email,
                      support_issue_id: support_issue.id)
  end
end

User.find(deleted_user).destroy
User.find(deleted_admin).destroy

SiteSetting.first.update(rich_text_support_issues: 'true')
SiteSetting.first.update(rich_text_support_replies: 'true')

users = User.all
3.times do |i|
  user  = users[i]
  following = users[i+1..15]
  followers = users[i+3..15]
  following.each { |followed| user.follow(followed) }
  followers.each { |follower| follower.follow(user) }
end
