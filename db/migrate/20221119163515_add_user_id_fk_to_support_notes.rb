class AddUserIdFkToSupportNotes < ActiveRecord::Migration[7.0]
  def change
    add_foreign_key :support_notes, :users, column: :user_id
  end
end
