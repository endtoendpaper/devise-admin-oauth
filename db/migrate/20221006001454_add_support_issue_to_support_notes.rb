class AddSupportIssueToSupportNotes < ActiveRecord::Migration[7.0]
  def change
    add_reference :support_notes, :support_issue, null: false, foreign_key: true
  end
end
