class AddSupportIssueToSupportComments < ActiveRecord::Migration[7.0]
  def change
    add_reference :support_comments, :support_issue, null: false, foreign_key: true
  end
end
