class AddUserEmailToSupportIssue < ActiveRecord::Migration[7.0]
  def change
    add_column :support_issues, :user_email, :string
  end
end
