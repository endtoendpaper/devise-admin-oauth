class CreateTables < ActiveRecord::Migration[8.0]
  def change
    create_table :tables do |t|
      t.integer :columns, default: 1
      t.integer :rows, default: 1
      t.json :data, default: {}
      t.string :record_type
      t.bigint :record_id
      t.boolean :header_row, default: false
      t.boolean :record_saved, default: false

      t.timestamps
    end

    add_index :tables, [:record_saved]
  end
end
