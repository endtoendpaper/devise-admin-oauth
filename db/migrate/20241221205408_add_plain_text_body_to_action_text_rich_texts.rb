class AddPlainTextBodyToActionTextRichTexts < ActiveRecord::Migration[8.0]
  def change
    add_column :action_text_rich_texts, :plain_text_body, :text
    add_index :action_text_rich_texts,"to_tsvector('english', plain_text_body)", using: :gin, name: "tsvector_rich_text_idx"
  end
end
