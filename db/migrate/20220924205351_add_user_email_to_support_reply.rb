class AddUserEmailToSupportReply < ActiveRecord::Migration[7.0]
  def change
    add_column :support_replies, :user_email, :string
  end
end
