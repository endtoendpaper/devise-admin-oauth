class CreateSiteSettings < ActiveRecord::Migration[7.0]
  def change
    create_table :site_settings do |t|
      t.boolean :users_can_join, default: true
      t.string :site_name, default: "My New Site"
      t.boolean :display_site_name, default: true
      t.timestamps
    end
  end
end
