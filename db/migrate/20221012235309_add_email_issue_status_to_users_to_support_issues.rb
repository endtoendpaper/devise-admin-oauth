class AddEmailIssueStatusToUsersToSupportIssues < ActiveRecord::Migration[7.0]
  def change
    add_column :site_settings, :email_issue_status_to_users, :boolean, default: true
  end
end
