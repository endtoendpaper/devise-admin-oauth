class AddRichTextSupportRepliesToSiteSettings < ActiveRecord::Migration[7.0]
  def change
    add_column :site_settings, :rich_text_support_replies, :boolean, default: true
  end
end
