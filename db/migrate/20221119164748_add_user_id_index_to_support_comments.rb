class AddUserIdIndexToSupportComments < ActiveRecord::Migration[7.0]
  def change
    add_index :support_comments, :user_id
  end
end
