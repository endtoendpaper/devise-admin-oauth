class AddEmailContactFormToAdminsToSiteSettings < ActiveRecord::Migration[7.0]
  def change
    add_column :site_settings, :email_contact_form_to_admins, :boolean, default: true
  end
end
