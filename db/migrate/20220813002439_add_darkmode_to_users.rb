class AddDarkmodeToUsers < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :darkmode, :boolean, default: false
  end
end
