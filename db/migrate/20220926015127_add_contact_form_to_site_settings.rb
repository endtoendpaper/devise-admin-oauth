class AddContactFormToSiteSettings < ActiveRecord::Migration[7.0]
  def change
    add_column :site_settings, :contact_form, :boolean, default: true
  end
end
