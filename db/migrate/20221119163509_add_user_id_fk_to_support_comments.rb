class AddUserIdFkToSupportComments < ActiveRecord::Migration[7.0]
  def change
    add_foreign_key :support_comments, :users, column: :user_id
  end
end
