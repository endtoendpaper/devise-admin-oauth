class ChangeUserIdToBigintFromSupportComments < ActiveRecord::Migration[7.0]
  def change
    change_column(:support_comments, :user_id, :bigint)
  end
end
