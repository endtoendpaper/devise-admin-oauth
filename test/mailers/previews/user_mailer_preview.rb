# Preview all emails at http://localhost:3001/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3001/rails/mailers/user_mailer/support_reply
  def support_reply
    @admin = User.where(admin: true).first
    @user = User.last
    @support_issue = FactoryBot.create(:support_issue, :rich_text, user_id: @user.id)
    @support_reply = FactoryBot.create(:support_reply, :rich_text, user_id: @admin.id, support_issue_id: @support_issue.id)
    UserMailer.support_reply(@support_reply)
  end

  def support_issue_created
    @user = User.last
    @support_issue = FactoryBot.create(:support_issue, :rich_text, user_id: @user.id)
    UserMailer.support_issue_created(@support_issue)
  end

  def support_issue_reopened
    @user = User.last
    @support_issue = FactoryBot.create(:support_issue, :rich_text, user_id: @user.id)
    UserMailer.support_issue_reopened(@support_issue)
  end

  def support_issue_resolved
    @user = User.last
    @support_issue = FactoryBot.create(:support_issue, :rich_text, user_id: @user.id)
    UserMailer.support_issue_resolved(@support_issue)
  end
end
