# Preview all emails at http://localhost:3001/rails/mailers/admins_mailer
class AdminsMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3001/rails/mailers/admins_mailer/support_issue
  def support_issue_created
    @user = User.last
    @support_issue = FactoryBot.create(:support_issue, :rich_text, user_id: @user.id)
    AdminsMailer.support_issue_created(@support_issue, @user)
  end

  def support_comment
    @user = User.last
    @support_issue = FactoryBot.create(:support_issue, :rich_text, user_id: @user.id)
    @support_comment = FactoryBot.create(:support_comment, :rich_text, user_id: @user.id, support_issue_id: @support_issue.id)
    AdminsMailer.support_comment(@support_comment, @user)
  end

  def support_reply
    @admin = User.where(admin: true).first
    @user = User.last
    @support_issue = FactoryBot.create(:support_issue, :rich_text, user_id: @user.id)
    @support_reply = FactoryBot.create(:support_reply, :rich_text, user_id: @admin.id, support_issue_id: @support_issue.id)
    AdminsMailer.support_reply(@support_reply, @admin)
  end

  def support_note
    @admin = User.where(admin: true).first
    @user = User.last
    @support_issue = FactoryBot.create(:support_issue, :rich_text, user_id: @user.id)
    @support_note = FactoryBot.create(:support_note, :rich_text, user_id: @admin.id, support_issue_id: @support_issue.id)
    AdminsMailer.support_note(@support_note, @admin)
  end

  def support_issue_reopened
    @admin = User.where(admin: true).first
    @user = User.last
    @support_issue = FactoryBot.create(:support_issue, :rich_text, user_id: @user.id)
    AdminsMailer.support_issue_reopened(@support_issue, @admin)
  end

  def support_issue_resolved
    @admin = User.where(admin: true).first
    @user = User.last
    @support_issue = FactoryBot.create(:support_issue, :rich_text, user_id: @user.id)
    AdminsMailer.support_issue_resolved(@support_issue, @admin)
  end
end
