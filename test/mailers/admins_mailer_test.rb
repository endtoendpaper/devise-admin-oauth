require "test_helper"

class AdminsMailerTest < ActionMailer::TestCase
  test "support_issue" do
    mail = AdminsMailer.support_issue
    assert_equal "Support issue", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
